# CMB lensing and ISW cross-correlation with Legacy Survey

This repository contains the source code used to perform the analysis presented in Hang et al. (2020) ([arxiv:2010.00466](https://arxiv.org/abs/2010.00466))
If you have questions/queries then contact qhang@roe.ac.uk or hangqianjun007@gmail.com.

Along with all the codes we also provide a cleaned and systematic corrected density maps in tomographic bin which one can use for further analysis without needing to worry about phot-z details or download the full catalogue. We also provide the calibration table for photo-z and hence one can get a photo-z estimate given legacy survey photometry without needing to re-perform the calibration by themselves. Please note that the input photometry should be extinction corrected. A fits table of galaxies with their RA, DEC, photo-z produced in this work and in Zhou et al. (2020) selected by magnitude limits g<24, r<22, w1<19.5 is also provided.

For photo-z calibration we have used the following surveys: GAMA (DR2; Liske et al. 2015), BOSS (DR12; Alamet al. 2015), eBOSS (DR16; Ahumada et al. 2020), VIPERS (DR2; Scodeggio et al. 2018), and DEEP2 (Newman et al. 2013). In addition, we also include COSMOS (Ilbert et al. 2009) and DESY1A1redMaGiC (Cawthon et al. 2018).

## Main data products:
 * Photo-z color calibration:\
   files: [data/infer_z_3D_gr_rz_zw1_v8_spec_highz_DES_COS_GAMArej.pkl](https://gitlab.com/qianjunhang/desi-legacy-survey-cross-correlations/-/blob/master/data/infer_z_3D_gr_rz_zw1_v8_spec_highz_DES_COS_GAMArej.pkl)\
   <b>Description:</b>This is the 3D colour grid used to assign photometric redshifts. This is a python dictionary stored using [pickle](https://docs.python.org/3/library/pickle.html) library, and can be read using pickle.load(filename) or dump_load(filename) in ["functions/ISW_functions.py"](https://gitlab.com/qianjunhang/desi-legacy-survey-cross-correlations/-/blob/master/functions/ISW_functions.py#L17). The actual calibration grid is stored under the key "matrix", and the colour bins for each axis is given under the key "[gr/rz/zw1]_bins". Unfilled cells have values set to -99. The magnitudes are defined with [Galactic extinction correction](https://www.legacysurvey.org/dr8/catalogs/#galactic-extinction-coefficients). This calibration grid can be used to estimate photoz for any new sample of galaxies as long as one is using the appropriate extinction correction and the object falls in our 3D colour cell which is not empty.
   
 
 * Galaxy samples with photo-z:\
   files:\
       * NGC: [data/Legacy_Survey_BASS-MZLS_galaxies-selection.fits](https://gitlab.com/qianjunhang/desi-legacy-survey-cross-correlations/-/blob/master/data/Legacy_Survey_BASS-MZLS_galaxies-selection.fits)\
       * SGC: [data/Legacy_Survey_DECALS_galaxies-selection.fits](https://gitlab.com/qianjunhang/desi-legacy-survey-cross-correlations/-/blob/master/data/Legacy_Survey_DECALS_galaxies-selection.fits)\
   <b>Description:</b> These are fits file and can be read using any fit reader library. Files contains position on the sky (RA,DEC) along with two different estimates of photo_z (PHOTOZ_3DINFER,PHOTOZ_ZHOU). Where PHOTOZ_3DINFER provides estimates of photoz obtained in this work. This file contains galaxy sample selected by magnitude limits g<24, r<22, w1<19.5.  

 * Galaxy density maps in tomographic bins:\
   files: data/galmap_final-bin[0/1/2/3].fits: [bin0](https://gitlab.com/qianjunhang/desi-legacy-survey-cross-correlations/-/blob/master/data/galmap_final-bin0.fits), [bin1](https://gitlab.com/qianjunhang/legacy-survey-cross-correlations/-/blob/master/data/galmap_final-bin1.fits), [bin2](https://gitlab.com/qianjunhang/legacy-survey-cross-correlations/-/blob/master/data/galmap_final-bin2.fits), [bin3](https://gitlab.com/qianjunhang/legacy-survey-cross-correlations/-/blob/master/data/galmap_final-bin3.fits)\
   <b>Description:</b>These are photometric maps for tomographic bins 0-3 with corrections. The four maps corresponds to the four tomographic bins as described in [Hang et. al. 2020](https://arxiv.org/abs/2010.00466). Please see Figure 3 for redshift distributions and Table 1 for sample properties.

 * Completeness map:\
   files: [data/Legacy_footprint_completeness_mask_128.fits](https://gitlab.com/qianjunhang/desi-legacy-survey-cross-correlations/-/blob/master/data/Legacy_footprint_completeness_mask_128.fits)\
   <b>Description:</b>This is the completeness map for the DESI Legacy Survey. These maps are derived by querying the pixel level mask from the legacy survey data which are then pixelized using healpix. 

 * Final mask:\
   files: [data/Legacy_footprint_final_mask.fits](https://gitlab.com/qianjunhang/desi-legacy-survey-cross-correlations/-/blob/master/data/Legacy_footprint_final_mask.fits)\
   <b>Description:</b>This is the final mask with cuts in completeness and stellar density.

All maps are stored as healpix maps with nside=1024 and RING ordering.


## Steps for the full analysis
The pipeline requires the following python packages:\
    - healpy\
    - fitsio\
    - astropy\
    Our power spectrum is generated from CAMB.

* Legacy Survey catalgoues
    - The public version of legacy survey DR8 catalogues can be dowloaded from NERSc and NAOAO data lab
    - Pipeline for data selection and photo-z assginment is in ["functions/DECALS_BASS_MZLS_selection-fits.ipynb"](https://gitlab.com/qianjunhang/desi-legacy-survey-cross-correlations/-/blob/master/functions/DECALS_BASS_MZLS_selection-fits.ipynb)

* Photo-z calibration
    - Obtain Spectroscopic calibration samples
    - Match the calibration sample with Legacy survey Photometry
    - Photo-z inference table and code (inference table is supplied in pickle format "data/infer_z_3D_gr_rz_zw1_v8_spec_highz_DES_COS_GAMArej.pkl")

    - ####  You can skip the steps above and directly access our selected catalogue via the fits files "data/Legacy_Survey_[DECALS/BASS-MZLS]_galaxies-selection.fits".

* obtained raw density map in tomographic bins
    - This is step 1 in [functions/Legacy_Survey_analysis_pipeline.py](https://gitlab.com/qianjunhang/desi-legacy-survey-cross-correlations/-/blob/master/functions/Legacy_Survey_analysis_pipeline.py#L100) including the correction of completeness map ("data/Legacy_footprint_completeness_mask_128.fits").

* Obtain the systematic correction
    - We perform systematic correction to the galaxy density map using [functions/densitymap_galactic_correction.py](https://gitlab.com/qianjunhang/desi-legacy-survey-cross-correlations/-/blob/master/functions/densitymap_galactic_correction.py)
    - The [ALLWISE total density map](https://gitlab.com/qianjunhang/desi-legacy-survey-cross-correlations/-/blob/master/data/allwise_total_rot_1024.fits) is used for galactic correction. We used a smoothed version of the original map, which can be generated from: https://wise2.ipac.caltech.edu/docs/release/allsky. The original total density map was smoothed with a FWHM of 0.1 radian, then pixels were iteratively replaced where the original was a sufficiently large outlier from the smoothed map until the smoothed map converged.

    - #### You can directly use the density map ("data/galmap_final-bin[0/1/2/3].fits") along with the final mask ("data/Legacy_footprint_final_mask.fits") if needed and skip all the step before

The following steps are included in step 2 of [functions/Legacy_Survey_analysis_pipeline.py](https://gitlab.com/qianjunhang/desi-legacy-survey-cross-correlations/-/blob/master/functions/Legacy_Survey_analysis_pipeline.py#L322):

* Measure angular power spectrum $`C_{\ell}`$
    - The Planck 2018 lensing convergence and temperature maps used in this step need to be downloaded separately from: https://pla.esac.esa.int
    - In the $`C_{\ell}`$ measurement step, the errors are also estimated and this was validated against log-normal simulations.

* Parameter Constraints
    - The linear and non-linear powerspectrum generated by CAMB needs to be run separately
    - The best-fit photo-z distribution p(z) needs to be inputed
    - The code computes theoretical prediction for galaxy-galaxy, galaxy-lensing, and galaxy-temperature cross-correlation. Galaxy biases are fitted in the galaxy auto-correlation part, follwing the 2-bias model treatment in [Hang et. al. 2020](https://arxiv.org/abs/2010.00466). The code also fits a second order polynomial for the bias evolution for calculating the unbinned case. Alternatively you can supply your own b(z) in this step. These bias evolution curves are then fixed for the cross-correlation analysis.
    - The likelihood of $`A_{\kappa}`$ and $`A_{ISW}`$ is then computed from the ratio of data and theory (with the same galaxy bias) using the diagonal covariance.
* Cosmological interpretation
    - We first obtain dependence of $`A_{\kappa}`$ on cosmological parameter given our unbinned n(z). This is done by varying one parameter at a time using CAMB, and measure the dependence of $`C_{\ell}^{\delta k}`$ on these parameters. We find that it is proportional to the combination $`\sigma_8 \Omega_m^{0.78}`$.
    - We then use [functions/cosmology_interpretation.py](https://gitlab.com/qianjunhang/desi-legacy-survey-cross-correlations/-/blob/master/functions/cosmology_interpretation.py) to make the final cosmological interpretation plot.

    ![Final Constraints on \\( \Omega_m \\) and \\( \sigma_8 \\)](data/omsigma8.png)

If you use the code or data products on this site, please cite [Hang et. al. 2020](https://arxiv.org/abs/2010.00466).
