import numpy as np
import healpy as hp
import sys
import Useful_functions as uf
import mycosmo
import pylab as pl
from scipy.optimize import minimize

def gal_powerspectrum_corr(map1,map2,mask,nside=512,lmax=300,nell=10,print_out=True,type1=1,type2=2,full=False):
    npix=12*nside**2

    map1=map1*mask
    tot1=sum(mask)
    tot2=sum(map1)
    fsky=tot1/npix
    rntot1=np.copy(tot2)
    if print_out==True:
        print "f_sky = ", fsky
        print('map 1 total = ', rntot1)
    if type1==1: #like galaxy map
        map1=mask*(map1*tot1/tot2 - 1) #sum(map1)=0
    elif type1==2:#like delta map
        map1=mask*(map1-tot2/tot1)    

    map2=map2*mask
    tot1=sum(mask)
    tot2=sum(map2)
    rntot2=np.copy(tot2)
    if print_out==True:
        print('map2: total = ', rntot2)
    if type2==1: #like galaxy map
        map2=mask*(map2*tot1/tot2 - 1)
    elif type2==2:#like delta map
        map2=mask*(map2-tot2/tot1) 
    
    a1=hp.map2alm(map1,lmax=lmax)
    a2=hp.map2alm(map2,lmax=lmax)

    px=hp.alm2cl(a1,a2) #x=cross
    pk=hp.alm2cl(a1) #k=map1
    pg=hp.alm2cl(a2) #g=map2

    xx=np.zeros(lmax+1)

    nbins=lmax/nell
    #!
    #nbins=nbins-1
    #print(nbins)

    results1=np.zeros((nbins,2))
    results3=np.zeros((nbins,2))
    results5=np.zeros((nbins,2))
    results6=np.zeros((nbins,2))

    for i in range(lmax+1):
        xx[i]=i
        px[i]=i*(i+1)*px[i]/2./np.pi
        px[i]=px[i]/fsky
        pg[i]=i*(i+1)*pg[i]/2./np.pi
        pg[i]=pg[i]/fsky
        pk[i]=i*(i+1)*pk[i]/2./np.pi
        pk[i]=pk[i]/fsky
    
    if full==True:
        if type1==1:#only subtract when its galaxy like
            pshot=2*fsky*xx*(1+xx)/rntot1
            pk=pk-pshot
        if type2==1:
            pshot=2*fsky*xx*(1+xx)/rntot2
            pg=pg-pshot
        return xx,px,pk,pg
    
    #cross power of map1 and map2
    xb1=np.zeros(nbins)
    yb1=np.zeros(nbins)
    xe1=np.zeros(nbins)
    ye1=np.zeros(nbins)

    for i in range(nbins):
        tot=0
        tot2=0
        for j in range(nell):
            #!
            k=nell*i+j#+5
            tot=tot+px[k] #summing up the power in intervals of dl=10
            tot2=tot2+px[k]**2 #summing up the power^2 in intervals of dl=10
            #print(k)

        tot=tot/float(nell) #mean of the power in intervals of dl=10
        tot2=np.sqrt(tot2/float(nell) - tot*tot) #std of the power
        #!
        xb1[i]=(i+0.5)*nell#+5 #x-axis
        yb1[i]=tot #mean power
        xe1[i]=nell/2.
        nmodes=nell*(2*xb1[i]+1)/2.
        ye1[i]=tot2/np.sqrt(nell-1)/np.sqrt(fsky) #unbiased estimate of std

        results1[i,0]=yb1[i]
        results1[i,1]=ye1[i]
    
    #auto power of map1
    xb2=np.zeros(nbins)
    yb2=np.zeros(nbins)
    xe2=np.zeros(nbins)
    ye2=np.zeros(nbins)
    for i in range(nbins):
        tot=0
        tot2=0
        for j in range(nell):
            #!
            k=nell*i+j#+5
            tot=tot+pk[k]
            tot2=tot2+pk[k]**2

        tot=tot/float(nell)
        tot2=np.sqrt(tot2/float(nell) - tot*tot)
        #!
        xb2[i]=(i+0.5)*nell#+5
        yb2[i]=tot
        xe2[i]=nell/2.
        ye2[i]=tot2/np.sqrt(nell-1)/np.sqrt(fsky)

    #shot noise subtraction
    for i in range(nbins):
        if type1==1:#only subtract when its galaxy like
            pshot=2*fsky*xb2[i]*(1+xb2[i])/rntot1
            yb2[i]=yb2[i]-pshot

        results3[i,0]=yb2[i]
        results3[i,1]=ye2[i]
        
    #auto power of map2
    xb4=np.zeros(nbins)
    yb4=np.zeros(nbins)
    xe4=np.zeros(nbins)
    ye4=np.zeros(nbins)
    for i in range(nbins):
        tot=0
        tot2=0
        for j in range(nell):
            #!
            k=nell*i+j#+5
            tot=tot+pg[k]
            tot2=tot2+pg[k]**2

        tot=tot/float(nell)
        tot2=np.sqrt(tot2/float(nell) - tot*tot)
        #!
        xb4[i]=(i+0.5)*nell#+5
        yb4[i]=tot
        xe4[i]=nell/2.
        ye4[i]=tot2/np.sqrt(nell-1)/np.sqrt(fsky)
    
    #shot noise subtraction
    for i in range(nbins):
        if type2==1:
            pshot=2*fsky*xb4[i]*(1+xb4[i])/rntot2
            yb4[i]=yb4[i]-pshot

        results5[i,0]=yb4[i]
        results5[i,1]=ye4[i]
        
    #final cross ratio independent of bias
    r=results1[:,0]/np.sqrt(results5[:,0]*results3[:,0])
    dr=np.sqrt((results1[:,1]/results1[:,0])**2+0.5*(results5[:,1]/results5[:,0])**2+0.5*(results3[:,1]/results3[:,0])**2)
    results6[:,0]=r
    results6[:,1]=dr*r
    #!
    l=(np.arange(nbins)+0.5)*nell#+5
    
    #either return full or grouped
    return l,results1, results3, results5, results6


def auto_correlation(map1,mask,type1=1,nside=512,lmax=300,nell=10,print_out=True,full=False,shot_noise=True):
    npix=12*nside**2
    map1=map1*mask
    tot1=sum(mask)
    tot2=sum(map1)
    fsky=tot1/npix
    rntot1=np.copy(tot2)
    if print_out==True:
        print "f_sky = ", fsky
        print('map 1 total = ', rntot1)
    if type1==1: #like galaxy map
        map1=mask*(map1*tot1/tot2 - 1) #sum(map1)=0
    elif type1==2:#like delta map
        map1=mask*(map1-tot2/tot1)

    a1=hp.map2alm(map1,lmax=lmax)
    p1=hp.alm2cl(a1)
    xx=np.zeros(lmax+1)
    nbins=lmax/nell
    #!
    #nbins=nbins-1
    #print(nbins)

    for i in range(lmax):
        xx[i]=i
        p1[i]=i*(i+1)*p1[i]/2./np.pi
        p1[i]=p1[i]/fsky

    if full==True:
        if type1==1:#only subtract when its galaxy like
            pshot=2*fsky*xx*(1+xx)/rntot1
            p1=p1-pshot
        return xx,p1

    #auto power of map1
    xb2=np.zeros(nbins)
    yb2=np.zeros(nbins)
    xe2=np.zeros(nbins)
    ye2=np.zeros(nbins)
    for i in range(nbins):
        tot=0
        tot2=0
        for j in range(nell):
            #!
            k=nell*i+j#+5
            tot=tot+p1[k]
            tot2=tot2+p1[k]**2

        tot=tot/float(nell)
        tot2=np.sqrt(tot2/float(nell) - tot*tot)
        #!
        xb2[i]=(i+0.5)*nell#+5
        yb2[i]=tot
        xe2[i]=nell/2.
        nmodes=nell*(2*xb2[i]+1)/2.
        ye2[i]=tot2/np.sqrt(nell-1)/np.sqrt(fsky)
        #ye2[i]=tot2/np.sqrt(nmodes)/np.sqrt(fsky)

    #shot noise subtraction
    results3=np.zeros((nbins,2))
    for i in range(nbins):
        if type1==1:#only subtract when its galaxy like
            if shot_noise==True:
                pshot=2*fsky*xb2[i]*(1+xb2[i])/rntot1
                yb2[i]=yb2[i]-pshot

        results3[i,0]=yb2[i]
        results3[i,1]=ye2[i]
    
    #!
    l=(np.arange(nbins)+0.5)*nell#+5

    return l,results3    
    

def distance2cmb(om_m):
    z=np.linspace(0.01,1070,2000)
    dd=np.zeros(len(z))
    for ii in range(len(z)):
        dd[ii]=mycosmo.r_comoving(z[ii],100,om_m,1-om_m)
    return z,dd
        

def cl_gk_theory(nz,zbin=0,Omm=0.31,sig8=0.815,plot=True):
    #use mycosmo distance function
    #nz: column 0: z, column 1: nz, nz normalized by total number, but not bin width
    #sample in r space
    rmax=mycosmo.r_comoving(nz[-1,0],100,Omm,1-Omm)
    rmin=mycosmo.r_comoving(nz[0,0],100,Omm,1-Omm)
    #uniform r bins
    rr=np.linspace(rmin,rmax,50)
    rrcen=((rr+np.roll(rr,1))*0.5)[1:]
    dr=rr[1]-rr[0]
    zzb=uf.distance2z(rr,om_m=Omm,func=2)
    zzcen=((zzb+np.roll(zzb,1))*0.5)[1:]#take central z as the mean
    #need to interpolate the nz given the zzb
    cc=np.interp(zzcen,nz[:,0],nz[:,1])
    #normalize the nz
    cc=cc/(sum(cc)*dr)
    if(plot==True):
        pl.plot(rrcen,cc)
  
    #load power spectrum
    '''
    psfile=np.loadtxt('/disk1/qhang/GAMA/Halofit_sig8/halofitz=0sig8=08.dat')
    ksamp=psfile[:,0]
    psamp=psfile[:,2]*(sig8**2/0.8**2) #use non-linear, has z dependent shape at small scales, but ignored here
    #extend
    param=np.polyfit(np.log10(ksamp[:10]),np.log10(psamp[:10]),1)
    poly=np.poly1d(param)
    extk=np.linspace(-4,np.log10(ksamp.min()),30)
    extP=10**poly(extk)
    extk=10**extk
    ksamp=np.append(extk,ksamp)
    psamp=np.append(extP,psamp)   
    '''
    psfile=np.loadtxt('/disk2/qhang/ISW_CMB/LegacySurvey/halofit/P_nonlinear-bin%d.txt'%zbin)
    ksamp_raw=psfile[:,0]
    psamp_raw=psfile[:,1:]
    for ii in range(len(zzcen)):
        #extend to small k with power law
        param=np.polyfit(np.log10(ksamp_raw[:10]),np.log10(psamp_raw[:10,ii]),1)
        poly=np.poly1d(param)
        extk=np.linspace(-4,np.log10(ksamp_raw.min()),30)
        extP=10**poly(extk)
        if ii==0:
            psamp=np.append(extP,psamp_raw[:,ii])
        elif ii>0:
            usep=np.append(extP,psamp_raw[:,ii])
            psamp=np.column_stack((psamp, usep))
    extk=10**extk
    ksamp=np.append(extk,ksamp_raw)

    ell2=np.arange(1,301)
    
    #clgg:
    mell=np.outer(np.ones(len(rrcen)),ell2)
    mbincen=np.outer(rrcen,np.ones(len(ell2)))
    mcc=np.outer(cc,np.ones(len(ell2)))
    k=mell/mbincen
    pk=np.copy(k)
    for ii in range(len(zzcen)):
        usek=k[ii,:]
        pk[ii,:]=np.interp(usek,ksamp,psamp[:,ii])
    #pk=np.interp(k,ksamp,psamp)
    #g=np.outer(uf.growth_factor(zzcen),np.ones(len(ell2)))
    #agg=sum(mbincen*mcc**2*dr*pk*g**2)*np.pi/ell2
    agg=sum(mbincen*mcc**2*dr*pk)*np.pi/ell2

    #clkk: lensing kernel
    rls=mycosmo.r_comoving(1070,100,Omm,1-Omm)
    rmin=mycosmo.r_comoving(0.01,100,Omm,1-Omm)
    R=np.linspace(rmin,rls,300)
    z,dd=distance2cmb(Omm)
    Z=np.interp(R,dd,z)
    a=1/(1+Z)
    dR=R[1]-R[0]
    lensingk0=(3*100.**2*Omm/2./9e10)*R*(rls-R)/(a*rls)
    mEll=np.outer(np.ones(len(R)),ell2)
    mR=np.outer(R,np.ones(len(ell2)))
    K=mEll/mR
    PKls=np.interp(K,ksamp,psamp[:,0])
    G=np.outer(uf.growth_factor(1/a-1,Omega_m0=Omm),np.ones(len(ell2))) #This may be not quite right
    mlensk0=np.outer(lensingk0,np.ones(len(ell2)))
    akk=sum(mR*mlensk0**2*dR*PKls*G**2)*np.pi/ell2
    
    #clgk
    lensingk1=(3*100.**2*Omm/2./9e10)*rrcen*(rls-rrcen)/rls*(1+zzcen)
    mlensk1=np.outer(lensingk1,np.ones(len(ell2)))
    #agk=sum(mbincen*mcc*mlensk1*dr*pk*g**2)*np.pi/ell2
    agk=sum(mbincen*mcc*mlensk1*dr*pk)*np.pi/ell2

    return ell2,agg,akk,agk


def growthrate(z,Omega_m0=0.27,power=0.55):
    a=1./(1.+z)
    Omega_m=Omega_m0*a**(-3)/(Omega_m0*a**(-3)+1-Omega_m0)
    return Omega_m**power
'''
def hubble(z,om_m=0.27):
    factor=(0.75*om_m*1.52+1.52*(om_m**0.4/2)**1.52*z**0.52)/(1+0.75*om_m*1.52*z+(om_m**0.4*z/2)**1.52)
    doz=2997.92458/(1+0.75*om_m*1.52*z+(om_m**0.4*z/2)**1.52)**(1./1.52)
    d=np.zeros(len(z))
    for ii in range(len(z)):
        d[ii]=mycosmo.r_comoving(z[ii],100,om_m,1-om_m)
    res=doz-1/1.52*d*factor
    return 3e5/res
'''
def hubble(z,om_m=0.27):
    a=1./(1.+z)
    return 100*np.sqrt((om_m*a**(-3)+(1-om_m)))

def cl_gt_theory(Nz,zbin=0,Omm=0.31,sig8=0.815,h=0.67,linear=False):
    #Nz: column 0: z, column 1: nz normalized by total counts
    #linear: defualt: use non-linear power spectrum
    #'''
    if linear==True:
        fin=np.loadtxt('/disk1/qhang/Projects/ISW_CMB/halofit-z-0-sig8-0815-ext.txt')
        kk=fin[:,0]
        Plin=fin[:,1]/kk**3*2*np.pi**2#this is linear P

    
    if linear==False:
        psfile=np.loadtxt('/disk2/qhang/ISW_CMB/LegacySurvey/halofit/P_nonlinear-bin%d-gtbin.txt'%zbin)
        ksamp_raw=psfile[:,0]
        psamp_raw=psfile[:,1:]
        for ii in range(len(zbins)):
            #update power spectrum
            psamp_raw[:,ii]=psamp_raw[:,ii]/ksamp_raw**3*2*np.pi**2
            #extend to small k with power law
            param=np.polyfit(np.log10(ksamp_raw[:10]),np.log10(psamp_raw[:10,ii]),1)
            poly=np.poly1d(param)
            extk=np.linspace(-4,np.log10(ksamp_raw.min()),30)
            extP=10**poly(extk)
            if ii==0:
                psamp=np.append(extP,psamp_raw[:,ii])
            elif ii>0:
                usep=np.append(extP,psamp_raw[:,ii])
                psamp=np.column_stack((psamp, usep))
        extk=10**extk
        ksamp=np.append(extk,ksamp_raw)
    
    
    #this time we already have uniform binning in z
    zbins=np.linspace(Nz[0,0],Nz[-1,0],100)
    dz=zbins[1]-zbins[0]
    #zbins=Nz[:,0]
    print(len(zbins))
    #print(np.mean(zz))
    nz=np.interp(zbins,Nz[:,0],Nz[:,1])
    nz=nz/(sum(nz)*dz)
    
    if linear==True:
        #compute the growth factor
        gfactor=uf.growth_factor(zbins,Omega_m0=Omm)
        #In case we use nonlinear PS, the factor is included
        
    grate=growthrate(zbins,Omega_m0=Omm)#grwoth rate is Omm**0.6
    Hz=hubble(zbins,om_m=Omm)#*h
    d=np.zeros(len(zbins))
    for ii in range(len(zbins)):
        d[ii]=mycosmo.r_comoving(zbins[ii],100,Omm,1-Omm)

    if linear==True:
        integrand_k=Plin*(3./2.*(100./kk)**2*Omm)
        integrand_z=gfactor**2*(1-grate)*nz/(d)**2*Hz
    
    if linear==False:
        integrand_k=np.copy(psamp)
        for ii in range(len(zbins)):
            integrand_k[:,ii]=psamp[:,ii]*(3./2.*(100./ksamp)**2*Omm)
        integrand_z=(1-grate)*nz/(d)**2*Hz
    
    #for ii in range(len(integrand_k[0,:])):
        #if ii%20==0:
            #pl.loglog(ksamp,integrand_k[:,ii])
    #pl.plot(d,integrand_z)
    
    ell2=np.arange(1,301)
    
    if linear==False:
        integrand_k_interp=np.zeros((len(ell2),len(zbins)))
        for ii in range(len(zbins)):
            arg=ell2/d[ii]
            useinterp=np.interp(np.log10(arg),np.log10(ksamp),np.log10(integrand_k[:,ii]))
            integrand_k_interp[:,ii]=10**useinterp
        Cln=np.zeros(len(ell2))
        for jj in range(len(ell2)):
            Cln[jj]=sum(integrand_k_interp[jj,:]*integrand_z*dz*2)/(9e10)*2.73/(3e5)
    
    if linear==True:
        Cln=np.zeros(len(ell2))
        for ii in range(len(ell2)):
            arg=ell2[ii]/d
            integrand_k_interp=np.interp(np.log10(arg),np.log10(kk),np.log10(integrand_k))
            integrand_k_interp=10**integrand_k_interp
            Cln[ii]=sum(integrand_k_interp*integrand_z*dz*2)/(9e10)*2.73/(3e5)
    #pl.plot(d, integrand_k_interp[3,:],'.-')
    #print(sum(nz*dz))
    
    ell2=np.append(0,ell2)
    Cln=np.append(0,Cln)
    return ell2,Cln
    
def cl_to_wtheta(ell,cl,theta):
    #cl here not multiplied by l(l+1)/2pi
    #theta in degree
    arg=np.cos(theta*np.pi/180.)
    coeff=np.zeros(int(ell[-1])+1)#this has to start from ell=0
    for ii in range(len(ell)):
        ind=int(ell[ii])
        coeff[ind]=(2*ell[ii]+1)/(4*np.pi)*cl[ii]
    wtheta=np.polynomial.legendre.legval(arg,coeff)
    return wtheta

def theory_error(ell2,cl,mask):
    mean_clth=np.zeros(30)
    errth=np.zeros(30)
    xx=np.linspace(0,300,31)
    for kk in range(30):
        l=(kk+0.5)*10
        ind=(ell2>=xx[kk])&(ell2<xx[kk+1])
        mean_clth[kk]=sum(cl[ind])/10.
        nmodes=20*l/2.
        errth[kk]=mean_clth[kk]/np.sqrt(nmodes*sum(mask)/float(len(mask)))
    return np.c_[mean_clth,errth]

#+++++++New addition+++++#
def cl_integral_ef(ell,zbin,nz,zpk,k,pnlin,Omm=0.315,fitbz=False,bz=None):
    #nz now is an n-d array with axis=0: nz, axis=1: different zbin nz
    dz=zbin[1]-zbin[0]
    H=hubble(zbin,om_m=Omm)
    
    nbin=len(nz[0,:])
    sum_nz=np.sum(nz,axis=0)
    pz=nz/sum_nz[None,:]/dz
    
    if fitbz==True:
        if bz.size==len(zbin):
            pz=pz*bz[:,None]
        elif bz.size==len(zbin)*nbin:
            pz=pz*bz# in this case we allow small difference in each bin
            #print(pz.shape)
    
    #interpolate k, pnlin 2d function #to speed up, just load ps at the z rather than interpolate, 
    #once zbin is fixed
    from scipy import interpolate
    PS = interpolate.interp2d(zpk, k, pnlin, kind='cubic')
    
    rr=np.zeros(len(zbin))
    for ii in range(len(zbin)):
        rr[ii]=mycosmo.r_comoving(zbin[ii],100,Omm,1-Omm)
    
    ellm=np.outer(np.ones(len(zbin)),ell)
    rrm=np.outer(rr,np.ones(len(ell)))
    #argm=ellm/rrm
    #zbinm=np.outer(zbin,np.ones(len(ell)))

    interp_p=np.zeros((len(zbin),len(ell)))
    for ii in range(len(zbin)):
        interp_p[ii,:]=PS(zbin[ii],ell/rr[ii]).T
    
    Hm=np.outer(H,np.ones(len(ell)))
    
    cl={}
    for ii in range(nbin):
        for jj in range(nbin):
            if ii==jj:
                pzm=np.outer(pz[:,ii],np.ones(len(ell)))
                integrandm=interp_p*pzm**2*rrm*dz/(3e5)*Hm
                cl['%d%d'%(ii,ii)]=np.pi/ell*np.sum(integrandm,axis=0)
            if ii<jj:
                pz1m=np.outer(pz[:,ii],np.ones(len(ell)))
                pz2m=np.outer(pz[:,jj],np.ones(len(ell)))
                integrandm=interp_p*pz1m*pz2m*rrm*dz/(3e5)*Hm
                cl['%d%d'%(ii,jj)]=np.pi/ell*np.sum(integrandm,axis=0)
    return cl

def theory_format(ell2,clth,lmax=300,nell=10,fmt=1,err=False,fsky=1.):
    #fmt=1: same as data
    #fmt=2: use the ell
    nbins=lmax/nell
    mean_clth=np.zeros(nbins)
    errth=np.zeros(nbins)
    xx=np.linspace(0,lmax,nbins+1)
    ye2=np.zeros(nbins)
    if fmt==1:
        for kk in range(nbins):
            l=(kk+0.5)*nell
            ind=(ell2>=xx[kk])&(ell2<xx[kk+1])
            mean_clth[kk]=sum(clth[ind])/float(nell)
            if err==True:
                tot=sum(clth[ind])/float(nell)
                tot2=sum(clth[ind]**2)
                tot2=np.sqrt(tot2/float(nell) - tot*tot)  
                ye2[kk]=tot2/np.sqrt(nell-1)/np.sqrt(fsky)
        if err==True:
            mean_clth=np.c_[mean_clth,ye2]
    elif fmt==2:
        for kk in range(nbins):
            ind=ell2==ell[kk]
            mean_clth[kk]=clth[ind]
    return mean_clth

def clgk_integral(ell,zbin,nz1,zpk,k,pnlin,Omm=0.315,fitbz=False,bz=None):
    dz=zbin[1]-zbin[0]
    pz1=nz1/sum(nz1)/dz
    
    if fitbz==True:
        pz1=pz1*bz
    
    rls=mycosmo.r_comoving(1070,100,Omm,1-Omm)
    
    from scipy import interpolate
    PS = interpolate.interp2d(zpk, k, pnlin, kind='cubic')
    
    rr=np.zeros(len(zbin))
    for ii in range(len(zbin)):
        rr[ii]=mycosmo.r_comoving(zbin[ii],100,Omm,1-Omm)
    
    ellm=np.outer(np.ones(len(zbin)),ell)
    rrm=np.outer(rr,np.ones(len(ell)))
    zbinm=np.outer(zbin,np.ones(len(ell)))
    
    interp_p=np.zeros((len(zbin),len(ell)))
    for ii in range(len(zbin)):
        interp_p[ii,:]=PS(zbin[ii],ell/rr[ii]).T
    
    lensingkernel=(3*100.**2*Omm/2./9e10)*rrm*(rls-rrm)/rls*(1+zbinm)
       
    pz1m=np.outer(pz1,np.ones(len(ell)))
    integrandm=interp_p*pz1m*lensingkernel*rrm*dz
    cl=np.pi/ell*sum(integrandm)
    return cl

def clgt_integral(ell,zbin,nz1,zpk,k,pnlin,Omm=0.315,fitbz=False,bz=None):
    dz=zbin[1]-zbin[0]
    pz1=nz1/sum(nz1)/dz
    
    if fitbz==True:
        pz1=pz1*bz
    
    H=hubble(zbin,om_m=Omm)
    grate=growthrate(zbin,Omega_m0=Omm,power=0.55)
    
    from scipy import interpolate
    PS = interpolate.interp2d(zpk, k, pnlin, kind='cubic')
    
    rr=np.zeros(len(zbin))
    for ii in range(len(zbin)):
        rr[ii]=mycosmo.r_comoving(zbin[ii],100,Omm,1-Omm)
    
    ellm=np.outer(np.ones(len(zbin)),ell)
    rrm=np.outer(rr,np.ones(len(ell)))
    zbinm=np.outer(zbin,np.ones(len(ell)))
    Hm=np.outer(H,np.ones(len(ell)))
    gratem=np.outer(grate,np.ones(len(ell)))
    
    interp_p=np.zeros((len(zbin),len(ell)))
    for ii in range(len(zbin)):
        interp_p[ii,:]=PS(zbin[ii],ell/rr[ii]).T
    
    kkm=ellm/rrm
    
    iswkernel=(1-gratem)/rrm**2*Hm*(3./2.*(100./kkm)**2*Omm)/(3e5)/kkm**3*2.*np.pi**2
    pz1m=np.outer(pz1,np.ones(len(ell)))
    cl=sum(interp_p*pz1m*iswkernel*dz)*2/(9e10)*2.73
    cl=cl*ell*(ell+1)/2./np.pi
    
    return cl

def fit_function(param,x,y):
    g=1/np.sqrt(2*np.pi)/param[0]*np.exp(-(x-param[1])**2/(2*param[0]**2))
    return sum((y-g)**2)

def Gaussian(param,x):
    return 1/np.sqrt(2*np.pi)/param[0]*np.exp(-(x-param[1])**2/(2*param[0]**2))

def Ak_ellmax(data,theory,Ak,ellmax,fullL=False):
    L_ak_max=np.zeros((len(ellmax),2))
    full_L=np.zeros((len(ellmax),len(Ak)))
    for ll in range(len(ellmax)):
        indrange=[1,ellmax[ll]]
        bi=indrange[0]
        ei=indrange[1]
        
        y=theory[:,None]*Ak[None,:]
        d=data[:,0,None]
        e=data[:,1,None]
        #print(y.shape)
        chi2=np.sum((d[bi:ei]-y[bi:ei])**2/e[bi:ei]**2,axis=0)
        #print(chi2)
        dof=indrange[1]-indrange[0]
        L=np.exp(-(chi2-dof)/2.)
        dAk=Ak[1]-Ak[0]
        L=L/sum(L)/dAk
        full_L[ll,:]=L
        #pl.plot(Ak,L)
        #Now fit Gaussian to measure the mean and the sigma
        ind=np.argmax(L)
        p0=[0.03,Ak[ind]]
        res=minimize(fit_function,p0,args=(Ak,L))
        #print(res.x)
        L_ak_max[ll,0]=(res.x)[1]
        L_ak_max[ll,1]=(res.x)[0]
    if fullL==True:
        return full_L
    if fullL==False:
        return L_ak_max

def A_ellmax_multibins(Ak,ellmax,zbins,data,clgkth_bz,cltype='clgk',unbin=True):
    Ak_likelihood={}
    Ak_likelihood_stats={}
    L=1#combined likelihood
    for ii in range(len(zbins)-1):
        key=cltype+'%d'%ii
        Ak_likelihood_stats[key]=Ak_ellmax(data[key],clgkth_bz[key],Ak,ellmax)
        Ak_likelihood[key]=Ak_ellmax(data[key],clgkth_bz[key],Ak,ellmax,fullL=True)
        L=L*Ak_likelihood[key]
    
    #unbinned case
    if unbin==True:
        key=cltype+'%d'%(ii+1)
        Ak_likelihood_stats['unbin']=Ak_ellmax(data[key],clgkth_bz[key],Ak,ellmax)
        Ak_likelihood['unbin']=Ak_ellmax(data[key],clgkth_bz[key],Ak,ellmax,fullL=True)

    #now find combined likelihood
    tot=np.sum(L,axis=1)
    Ak_likelihood['product']=L/tot[:,None]/(Ak[1]-Ak[0])
    Ak_likelihood_stats['product']=np.zeros((len(ellmax),2))
    #measure mean and width of L
    for ll in range(len(ellmax)):
        ind=np.argmax(Ak_likelihood['product'][ll,:])
        p0=[0.03,Ak[ind]]
        res=minimize(fit_function,p0,args=(Ak,Ak_likelihood['product'][ll,:]))
        #print(res.x)
        Ak_likelihood_stats['product'][ll,0]=(res.x)[1]
        Ak_likelihood_stats['product'][ll,1]=(res.x)[0]

    Ak_likelihood['ellmax']=ellmax
    Ak_likelihood['A']=Ak
    Ak_likelihood_stats['ellmax']=ellmax
    Ak_likelihood_stats['A']=Ak
    return Ak_likelihood,Ak_likelihood_stats   


#iterative b(z) fitting here

def fit_2bias(bias,d1,d2lin,d2nl,sig):
    d2=d2lin*bias[0]**2+d2nl*bias[1]**2
    chi2=sum((d1-d2)**2/sig**2)
    return chi2

def fit_1bias(d1,d2,sig):
    v1=d1/d2
    s1=sig/d2
    w=1/s1**2
    w=w/sum(w)
    b2=sum(w*v1)
    return np.sqrt(b2)

def iterative_bz(bzlin,bznl,data,lmax,ellth,zbin,nz,zVect,kVect,pklin,pknl,omm,return_cl=False):
    cl_nl_bz=cl_integral_ef(ellth,zbin,nz,zVect,kVect,pknl,Omm=omm,fitbz=True,bz=bznl)
    cl_lin_bz_sub=cl_integral_ef(ellth,zbin,nz,zVect,kVect,pklin,Omm=omm,fitbz=True,bz=bznl)
    cl_lin_bz=cl_integral_ef(ellth,zbin,nz,zVect,kVect,pklin,Omm=omm,fitbz=True,bz=bzlin)

    N=len(nz[0,:])
    bias1_iter=np.zeros(N)
    bias2_iter=np.zeros(N)
    indmax=lmax/10
    for ii in range(N):
        key='%d%d'%(ii,ii)
        temp1=cl_nl_bz[key]-cl_lin_bz_sub[key]
        temp2=cl_lin_bz[key]
        f1=theory_format(ellth,temp1,lmax=lmax,nell=10,fmt=1)
        f2=theory_format(ellth,temp2,lmax=lmax,nell=10,fmt=1)
        p0=[1,1]
        usedata=data[key][:indmax]
        res=minimize(fit_2bias,p0,args=(usedata[1:,0],f2[1:],f1[1:],usedata[1:,1]))
        bias1_iter[ii]=res.x[0]
        bias2_iter[ii]=res.x[1]
    print(bias1_iter,bias2_iter)
    if return_cl==False:
        return bias1_iter, bias2_iter
    if return_cl==True:
        cl_bz={}
        for key in cl_nl_bz.keys():
            temp=cl_nl_bz[key]-cl_lin_bz_sub[key]+cl_lin_bz[key]
            cl_bz[key]=theory_format(ellth,temp,lmax=lmax,nell=10,fmt=1)
        #off digonal, return correlation coefficients:
        for ii in range(N):
            for jj in range(N):
               if ii<jj:
                   key='%d%d'%(ii,jj)
                   key1='%d%d'%(ii,ii)
                   key2='%d%d'%(jj,jj)
                   cl_bz[key]=cl_bz[key]/np.sqrt(cl_bz[key1]*cl_bz[key2])
        return cl_bz

