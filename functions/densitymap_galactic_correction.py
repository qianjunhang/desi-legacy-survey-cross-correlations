'''
This file applies corrections to the constrcuted raw galaxy maps.
Correction using star density and completeness map.
Correct by completeness first then stellar density.
'''
#import python libraries
import numpy as np
import pylab as pl
import matplotlib
matplotlib.rcParams.update({'font.size': 12, 'font.family': 'serif','axes.linewidth':1})
#matplotlib.use('agg')
#pl.switch_backend('agg')
import mycosmo

from matplotlib.colors import LogNorm
#import my functions
import ISW_functions as iswf
import healpy as hp
import cross_ps as cp


savedir='LegacySurvey/pipeline/'

#nside of the healpix map, default is 1024
nside=1024

fmask='../data/Legacy_footprint_completeness_mask_128.fits'
weight=hp.read_map(fmask)
weight=hp.ud_grade(weight,nside)

#####stellar density map
stellarmap=hp.read_map('allwise_total_rot_1024.fits')#ALLWISE total density map
stellarmap=hp.ud_grade(stellarmap,nside)
#for conservation of total stellar number
stellarmap=stellarmap*(1024/nside)**2

####produce the mask
#This mask uses weight>=0.86, and Nstar<12889/deg^2
threshold1=12889.*(360**2/np.pi)/(12*nside**2)
mask=np.zeros(len(weight))
ind=(stellarmap<threshold1)&(weight>=0.86)
mask[ind]=1
#save the mask
hp.writemap('../data/Legacy_footprint_final_mask.fits',mask)
weight=weight*mask
stellarmap=stellarmap*mask

#####E(B-V) map (not used here as no significant trend)
#ebvmap=hp.read_map('/disk2/qhang/ISW_CMB/LegacySurvey/ebv_ring_rot_512.fits')
#tot1=sum(ebvmap)
#ebvmap=hp.ud_grade(ebvmap,nside)
#ebvmap=ebvmap*mask

ra,dec=hp.pix2ang(nside,np.arange(len(mask)),lonlat=True)

#completeness=1 #0=not corrected by completeness, 1=corrected by completeness
skip=0
completeness=0

#if the map is not corrected by completeness:
if completeness==1:
    lab='comp'
    densmap={}
    print('Load raw galaxy map.')
    #correct for completeness here
    galmap=iswf.dump_load('galmap_raw.pkl')#load the raw galaxy counts
    for ii in range(4):
        temp=galmap[ii]*mask
        temp[mask.astype(bool)]=galmap[ii][mask.astype(bool)]/weight[mask.astype(bool)]
        #generate density map
        mean=sum(temp)/float(sum(mask))
        densmap[ii]=temp/mean-1
        densmap[ii]=densmap[ii]*mask  
    print('Galaxy map weighted by completeness. Density map generated.')

#if the map has been corrected by completeness (from pipeline):
elif completeness==0:
    densmap=iswf.dump_load(savedir+'densmap.pkl')
    galmap=iswf.dump_load(savedir+'galmap.pkl')

#redshift bins
zbins=[0,0.3,0.45,0.6,1.1]

#check the cross-correlation with both maps
test=0
if test==1:
    nplots=len(zbins)-1
    for kk in range(2):
        if kk==0:
            usecorr=weight
        if kk==1:
            usecorr=stellarmap
        fig,axarr=pl.subplots(1,nplots,figsize=[12,3],sharey=True)
        for ii in range(nplots):
            ell,c12,a11,a22,r=cp.gal_powerspectrum_corr(galmap[ii],usecorr,mask,type1=1,type2=2,nside=nside,lmax=300,nell=10,print_out=False)
            pl.sca(axarr[ii])
            pl.errorbar(ell,c12[:,0],yerr=c12[:,1])
        pl.subplots_adjust(wspace=0)
    pl.show()


###Look at the mean density in bins of weight and stellar density
#useebv=ebvmap[mask.astype(bool)]
useweight=weight[mask.astype(bool)]
usestellar=stellarmap[mask.astype(bool)]

stellar_bins=np.linspace(np.log10(usestellar.min()),np.log10(usestellar.max()),15)
weight_bins=np.linspace(useweight.min(),1.0,15)

if(0):
    cc=pl.hist(np.log10(usestellar), bins=stellar_bins,histtype='step')
    pl.xlabel('log star')
    pl.ylabel('number of pixels')
    pl.show()
    pl.close()

    cc=pl.hist(useweight, bins=weight_bins,histtype='step')
    pl.xlabel('completeness')
    pl.ylabel('number of pixels')
    pl.show()


###Density correction
mean_correct=np.zeros(len(zbins)-1)
usedens2={}

#re-evaluate mean at pixels with weight>0.95 and Nstar<8515
threshold2=8515.*(360**2/np.pi)/(12*nside**2)
for jj in range(len(zbins)-1):
    hpmap=densmap[jj]*mask
    usedens=hpmap[mask.astype(bool)]
    #renorm dens
    ind=(useweight>0.95)&(np.log10(usestellar)<np.log10(threshold2))
    mean=np.mean(usedens[ind]+1)
    print(mean)
    usedens2[jj]=(usedens+1)/mean-1
    mean_correct[jj]=mean

fit_fun={}
if skip==0:
    outstar=np.zeros((len(stellar_bins)-1,2*(len(zbins)-1)))
    outweight=np.zeros((len(weight_bins)-1,2*(len(zbins)-1)))
    for jj in range(len(zbins)-1):
        delta_star=np.zeros((len(stellar_bins)-1))
        std=np.zeros((len(stellar_bins)-1))
        for ii in range(len(stellar_bins)-1):
            x=np.log10(usestellar)
            ind=(x>=stellar_bins[ii])&(x<stellar_bins[ii+1])
            delta_star[ii]=np.mean(usedens2[jj][ind])
            std[ii]=np.std(usedens2[jj][ind])/np.sqrt(len(usedens2[jj][ind]))
        #out
        outstar[:,2*jj:(2*(jj+1))]=np.c_[delta_star,std]
        #save
    np.savetxt(savedir+'delta_comp_cor_vs_star.txt',outstar)
elif skip==1:
    outstar=np.loadtxt(savedir+'delta_comp_cor_vs_star.txt')
for jj in range(len(zbins)-1):
    delta_star=outstar[:,2*jj]
    std=outstar[:,2*jj+1]

    #plot density as a function of stellar density
    dx=stellar_bins[1]-stellar_bins[0]
    x=(stellar_bins+0.5*dx)[:-1]
    pl.errorbar(x[1:],delta_star[1:],yerr=std[1:],label='bin %d'%jj)

    #fit polynomial
    fitx=x
    fity=np.copy(delta_star)
    fity[0]=0
    fitw=1/np.copy(std)
    fitw[0]=1/std[1]
    param=np.polyfit(fitx,fity,5,w=fitw)
    fitf=np.poly1d(param)
    fit_fun[jj]=fitf
    pl.plot(x,fitf(x),'--',color='C%d'%jj)   
pl.xlabel('log10(stellar)')
pl.ylabel('delta')
pl.legend()
pl.show()
pl.close()

#exit()
    
####Now correct the density maps
densmap_corr={}
galmap_corr={}
densmap_smth_corr={}

for jj in range(len(zbins)-1):
    tot1=sum(galmap[jj]*mask)
    mean_counts=tot1/float(sum(mask))
    densmap_corr[jj]=((densmap[jj]+1)/mean_correct[jj]-1)*mask
    #correct
    fitf=fit_fun[jj]
    ind=np.where(mask>0)[0]
    ddens=fitf(np.log10(stellarmap[ind]))
    densmap_corr[jj][ind]=(densmap_corr[jj][ind]-ddens)
    densmap_corr[jj]=densmap_corr[jj]*mask
    galmap_corr[jj]=(densmap_corr[jj]+1)*mean_counts*mask

#save
#corrected density map
iswf.dump_save(densmap_corr,savedir+'densmap_final.pkl')
#corresponding galaxy map
iswf.dump_save(galmap_corr,savedir+'galmap_final.pkl')

       
#compute cross-correlation with corrected and un-corrected density
for kk in range(2):
    fig,axarr=pl.subplots(1,len(zbins)-1,figsize=[14,3],sharey=True)
    if kk==0:
        usemap=stellarmap*mask
    if kk==1:    
        usemap=weight*mask
    for ii in range(len(zbins)-1):
        pl.sca(axarr[ii])
        ell,c_star_corr,a_dens,a_star,r = cp.gal_powerspectrum_corr(galmap_corr[ii]*mask, usemap, mask, type1=1, type2=2, nside=nside, lmax=500, nell=10)
            pl.plot(ell,ell*0,'k-')
            pl.errorbar(ell,c_star_corr[:,0],yerr=c_star_corr[:,1])
        pl.subplots_adjust(wspace=0)
        if kk==0:
            pl.title('Cross-correlation with stellar map')
            #pl.savefig(savedir+'cross_star.png')
        if kk==1:
            pl.title('Cross-correlation with completeness map')
            #pl.savefig(savedir+'cross_weight.png')
    pl.show()
exit()
