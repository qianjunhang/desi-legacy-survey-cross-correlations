'''
This file is an example pipeline for:
1. generating the galaxy maps weighted by completeness
2. compute the auto- and cross-correlation
3. compute theory prediction for the auto- and cross-correlations, given the redshift distribution p(z).
4. compute the likelihood of A_kappa, A_ISW for the given p(z).
###Note that the galaxy maps are output as pickle files.
'''

#import python libraries
import numpy as np
import pylab as pl
import matplotlib
matplotlib.rcParams.update({'font.size': 12, 'font.family': 'serif','axes.linewidth':1})
import sys
from scipy.optimize import minimize
from scipy.ndimage import gaussian_filter1d

import ISW_functions as iswf
import healpy as hp
import fitsio as F
import cross_ps as cp
import mycosmo
import plot_Cls

#=================
###input: fits file of the north and south regions, generated from the Legacy Survey database
fnameS='output/data_matrix/Legacy_Survey_DECALS_galaxies.fits'
fnameN='output/data_matrix/Legacy_Survey_BASS-MZLS_galaxies.fits'

fmask='../data/Legacy_footprint_final_mask.fits'
mask=hp.read_map(fmask)
fmask='../data/Legacy_footprint_completeness_mask_128.fits'#final mask
weight=hp.read_map(fmask)
weight=weight*mask

#default nside=1024
nside=1024
print('nside=%d'%nside)
ipix=np.arange(12*nside**2)
mapra,mapdec=hp.pix2ang(nside,ipix,lonlat=True)

if nside!=1024:
    print('udgrade mask and weight to nside=%d'%nside)
    mask=hp.ud_grade(mask,nside)
    weight=hp.ud_grade(weight,nside)

masked_pix=ipix[mask.astype(bool)]
totmask=sum(mask)

#save the output here
savedir='LegacySurvey/pipeline/'
print('savedir=',savedir)

#north and south masks
northind=(mapdec>33).astype(int)
southind=(mapdec<=33).astype(int)
northmask=mask*northind
southmask=mask*southind
totnorth=sum(northmask)
totsouth=sum(southmask)

usez=1
#select which photo-z to use
#z_sig, z_a are the parameters for photoz, fitted from spec-z samples.
if usez==0:
    zkey='PHOTOZ'#Zhou et al. 2020
    z_sig=[0.0075,0.0128,0.0150,0.0248]
    z_a=[1.320,1.484,1.700,1.502]
elif usez==1:
    zkey='3DINFERZ'#infered from 3D grid
    z_sig=[0.0123,0.0151,0.0155,0.0265]
    z_a=[1.257,1.319,1.476,2.028]

#redshift bin edges
zbins=[0,0.3,0.45,0.6,0.8]
print('zbins: ',zbins)

#magnitude cuts
gcut=24
rcut=22
w1cut=19.5

###input: cosmology
omega_m=0.315
sig8=0.811
h=0.674

#skipping steps
load_pkl=1 #0=run step 1, 1=skip step 1
skip_corr=0 #0=run step 2
#===================

#===================
#functions
def lorentzian(x,sigma,a):
    return 1/(1+(x/sigma)**2/(2*a))**a
#===================

#step 1: make galaxy maps, density maps, n(z), save
#south
#skip this if you already have the galmap, nz, meanz, and densmap_smth
if load_pkl==0:
    fin=F.FITS(fnameS)
    if usez==0:
        sel=fin[1].where('DEC<=33 && MAG_G<%f && MAG_R<%f && MAG_W1<%f && MAG_Z<21'%(gcut,rcut,w1cut))
    elif usez==1:
        sel=fin[1].where('DEC<=33 && MAG_G<%f && MAG_R<%f && MAG_W1<%f'%(gcut,rcut,w1cut))    
    #give a DES redMaGiC-like selecion:
    if redMaGiC==1:
        g=fin[1]['MAG_G'][sel]
        r=fin[1]['MAG_R'][sel]
        z=fin[1]['MAG_Z'][sel]
        w1=fin[1]['MAG_W1'][sel]
        xcoord=g-r
        ycoord=r-z
        zcoord=g-w1
        sel2=iswf.DES_like_coloursel(xcoord,ycoord,zcoord)
        sel=sel[sel2]

    redshift=fin[1][zkey][sel]
    south_map={}
    south_nz={}
    south_meanz=np.zeros(len(zbins)-1)
    south_meandens=np.zeros(len(zbins)-1)
    Nsouth_raw=np.zeros(len(zbins)-1)
    Nsouth=np.zeros(len(zbins)-1)
    print('Step 1: generating galaxy maps and nz for south...')
    for ii in range(len(zbins)-1):
        zsel=(redshift>zbins[ii])&(redshift<=zbins[ii+1])
        rasel=fin[1]['RA'][sel][zsel]
        decsel=fin[1]['DEC'][sel][zsel]
        
        #make galaxy number counts map
        map1,pix1=iswf.hp_pixelize(rasel,decsel,nside=nside,nest=False,plots=0,title='',return_pix=True)
        #completeness correction
        south_map[ii]=np.zeros(len(mask))
        south_map[ii][mask.astype(bool)]=map1[mask.astype(bool)]/weight[mask.astype(bool)]

        #'''
        usepix=np.in1d(pix1,masked_pix)
        cc=np.histogram(redshift[zsel][usepix],bins=300,range=[0,1.2])
        print(len(redshift[zsel][usepix]))
        Nsouth_raw[ii]=len(redshift[zsel][usepix])
        dz=cc[1][1]-cc[1][0]

        bins=((cc[1]+np.roll(cc[1],1))*0.5)[1:]
        south_nz[ii]=np.c_[bins,cc[0]]
        #Nsouth[ii]=sum(south_map[ii]*southmask)
        Nsouth[ii]=sum(south_map[ii])
        south_meanz[ii]=np.mean(redshift[zsel][usepix])
        #south_meandens[ii]=sum(south_map[ii]*southmask)/float(totsouth)
        #'''
    #print(south_meanz)
    #print(south_meandens)

    #north
    fin=F.FITS(fnameN)
    if usez==0:
        sel=fin[1].where('DEC>33 && MAG_G<%f && MAG_R<%f && MAG_W1<%f && MAG_Z<21'%(gcut,rcut,w1cut))
    elif usez==1:
        sel=fin[1].where('DEC>33 && MAG_G<%f && MAG_R<%f && MAG_W1<%f'%(gcut,rcut,w1cut))   
    #redMaGiC:
    if redMaGiC==1:
        g=fin[1]['MAG_G'][sel]
        r=fin[1]['MAG_R'][sel]
        z=fin[1]['MAG_Z'][sel]
        w1=fin[1]['MAG_W1'][sel]
        xcoord=g-r
        ycoord=r-z
        zcoord=g-w1
        sel2=iswf.DES_like_coloursel(xcoord,ycoord,zcoord)
        sel=sel[sel2]

    redshift=fin[1][zkey][sel]
    north_map={}
    north_nz={}
    north_meanz=np.zeros(len(zbins)-1)
    north_meandens=np.zeros(len(zbins)-1)
    Nnorth_raw=np.zeros(len(zbins)-1)
    Nnorth=np.zeros(len(zbins)-1)
    print('Step 1: generating galaxy maps and nz for north...')
    for ii in range(len(zbins)-1):
        zsel=(redshift>zbins[ii])&(redshift<=zbins[ii+1])
        rasel=fin[1]['RA'][sel][zsel]
        decsel=fin[1]['DEC'][sel][zsel]

        #make galaxy number counts map
        map1,pix1=iswf.hp_pixelize(rasel,decsel,nside=nside,nest=False,plots=0,title='',return_pix=True)
        #completeness correction
        north_map[ii]=np.zeros(len(mask))
        north_map[ii][mask.astype(bool)]=map1[mask.astype(bool)]/weight[mask.astype(bool)]
        
        #z bins
        usepix=np.in1d(pix1,masked_pix)
        cc=np.histogram(redshift[zsel][usepix],bins=300,range=[0,1.2])
        print(len(redshift[zsel][usepix]))
        Nnorth_raw[ii]=len(redshift[zsel][usepix])
        dz=cc[1][1]-cc[1][0]

        bins=((cc[1]+np.roll(cc[1],1))*0.5)[1:]
        north_nz[ii]=np.c_[bins,cc[0]]
        #Nnorth[ii]=sum(north_map[ii]*northmask)
        Nnorth[ii]=sum(north_map[ii])
        north_meanz[ii]=np.mean(redshift[zsel][usepix])
        #north_meandens[ii]=sum(north_map[ii]*northmask)/float(totnorth)
        
    '''
    #total z distribution:
    #-----plot-----#
    pl.plot(bins,north_totnz/float(sum(north_totnz)),label='North')
    pl.plot(bins,south_totnz/float(sum(south_totnz)),label='South')
    pl.legend()
    pl.show()
    pl.close()    
    #-----plot-----# 
    '''
    #combine and save
    galmap={}
    densmap={}
    nz={}
    meanz=np.zeros(len(zbins)-1)
    for ii in range(len(zbins)-1):
        totmap=north_map[ii]+south_map[ii] #so that all pixels are filled with galaxies
        nn=totmap*northmask
        totnn=sum(nn)
        north_meandens[ii]=totnn/float(totnorth)
        ss=totmap*southmask
        totss=sum(ss)
        south_meandens[ii]=totss/float(totsouth)
        densmap[ii]=(nn/north_meandens[ii]-1)*northmask+(ss/south_meandens[ii]-1)*southmask
        
        meandens=(totnn+totss)/float(totmask)
        galmap[ii]=(densmap[ii]+1)*meandens*mask #so that sum of galaxies are the same
        
        #'''
        #dz=south_nz[ii][1,0]-south_nz[ii][0,0]
        nz[ii]=np.c_[south_nz[ii][:,0],(south_nz[ii][:,1]+north_nz[ii][:,1])]#/sum(south_nz[ii][:,1]+north_nz[ii][:,1])/dz]
        meanz[ii]=sum(nz[ii][:,0]*nz[ii][:,1]/sum(nz[ii][:,1]))
        #'''
    #'''
    D=np.zeros(len(zbins)-1)
    for ii in range(len(D)):
        D[ii]=mycosmo.r_comoving(meanz[ii],100,omega_m,1-omega_m)
    #'''

    iswf.dump_save(galmap,savedir+'galmap.pkl')
    iswf.dump_save(densmap,savedir+'densmap.pkl')
    iswf.dump_save(nz,savedir+'nz.pkl')
    iswf.dump_save(meanz,savedir+'meanz.pkl')

    #write a file to record parameters and other basic info
    myfile=open(savedir+'basic.txt','w')
    myfile.write('Redshift used: %s \n'%zkey)
    myfile.write('zbins: %s \n'%zbins)
    myfile.write('gcut: %s, rcut: %s, w1cut: %s \n\n'%(gcut,rcut,w1cut))
    myfile.write('redMaGiC-like coloursel: %s \n\n'%redMaGiC)
    
    myfile.write('Number of galaxies in north (raw): %s \n'%Nnorth_raw)
    myfile.write('Number of galaxies in north (weighted): %s \n'%Nnorth)
    myfile.write('Mean z north: %s \n'%north_meanz)
    myfile.write('Mean density north: %s \n\n'%north_meandens)
    
    myfile.write('Number of galaxies in south (raw): %s \n'%Nsouth_raw)
    myfile.write('Number of galaxies in south (weighted): %s \n'%Nsouth)
    myfile.write('Mean z south: %s \n'%south_meanz)
    myfile.write('Mean density south: %s \n\n'%south_meandens)
    
    myfile.write('Density smooth scale: %s \n'%sigma_com)
    myfile.write('omega_m: %s \n'%omega_m)
    myfile.write('sigma 8: %s \n'%sig8)
    myfile.write('Hubble h: %s \n'%h)
    myfile.close()

if load_pkl==1:
    print('step 1 skiped, loading maps...')
    galmap=iswf.dump_load(savedir+'galmap_final.pkl')
    densmap=iswf.dump_load(savedir+'densmap_final.pkl')

if(0):
    #------plot-----#
    #pl.figure(1)
    hp.mollview(densmap_smth[0],cmap='RdBu_r',max=0.5,min=-0.5,title='$%s<z\\leq%s$'%(zbins[0],zbins[1]),unit='$\\delta$')
    #pl.show()
    pl.savefig(savedir+'densmap_smth_bin0.pdf',bbox_inches="tight")
    pl.close()
    
    hp.mollview(densmap_smth[1],cmap='RdBu_r',max=0.5,min=-0.5,title='$%s<z\\leq%s$'%(zbins[1],zbins[2]),unit='$\\delta$')
    #pl.show()
    pl.savefig(savedir+'densmap_smth_bin1.pdf',bbox_inches="tight")
    pl.close()
    
    hp.mollview(densmap_smth[2],cmap='RdBu_r',max=0.5,min=-0.5,title='$%s<z\\leq%s$'%(zbins[2],zbins[3]),unit='$\\delta$')
    #pl.show()
    pl.savefig(savedir+'densmap_smth_bin2.pdf',bbox_inches="tight")
    pl.close()
    
    hp.mollview(densmap_smth[3],cmap='RdBu_r',max=0.5,min=-0.5,title='$%s<z\\leq%s$'%(zbins[3],zbins[4]),unit='$\\delta$')
    #pl.show()
    pl.savefig(savedir+'densmap_smth_bin3.pdf',bbox_inches="tight")
    pl.close()
    #------plot-----#
if(0):
    #-----plot-----#
    pl.figure(figsize=[6,4])
    for ii in range(len(zbins)-1):
        dz=nz[ii][1,0]-nz[ii][0,0]
        pl.fill_between(nz[ii][:,0],nz[ii][:,1]/float(sum(nz[ii][:,1]))/dz,nz[ii][:,0]*0,color='C%d'%ii,alpha=0.3)
        #pl.plot(nz_convl[ii][:,0],nz_convl[ii][:,1]/float(sum(nz_convl[ii][:,1]))/dz,'--',color='C%d'%ii,label='bin %d'%ii)
    pl.xlim([0,1.1])
    pl.ylim([0,9])
    pl.xlabel('Redshift')
    pl.ylabel('$p(z)$')
    pl.legend()
    pl.savefig(savedir+'nz.pdf',bbox_inches='tight')
    pl.show()
    pl.close()
    #-----plot-----#

#exit()

#step 2: generate auto and cross-correlations, as well as theory
#default lamx=500
lmax=500

#unbinned case
gmaptot=np.zeros(len(mask))
for ii in range(len(zbins)-1):
    gmaptot+=galmap[ii]
#Now add unbinned map to the last entry of galmap
galmap[len(zbins)-1]=gmaptot*mask

skip_corr=0
if skip_corr==0:
    data={}
    print('step 2: doing auto-correlation and cross-correlation between different z bins...')
    #here compute and save to files
    for ii in range(len(zbins)-1):
        for jj in range(len(zbins)-1):
            if ii<jj:
                ell,a01,a00,a11,r01=cp.gal_powerspectrum_corr(galmap[ii],galmap[jj],mask,nside=nside,lmax=lmax,nell=10,print_out=False,type1=1,type2=1,full=False)
                data['%d%d'%(ii,jj)]=r01
                data['%d%d'%(jj,jj)]=a11
                if jj==1:
                    data['%d%d'%(ii,ii)]=a00
    #deal with shot noise in allxbins
    fsky=sum(mask)/float(len(mask))
    Ntot=sum(galmap[len(zbins)-1])
    shotnoise=2*ell*(ell+1)*fsky/Ntot
    for ii in range(len(zbins)-1):
        ell,a01,a00,a11,r01=cp.gal_powerspectrum_corr(galmap[ii],galmap[len(zbins)-1],mask,nside=nside,lmax=lmax,nell=10,print_out=False,type1=1,type2=1,full=False)
        #correct for shot noise
        temp=a01[:,0]-shotnoise
        temp=temp/np.sqrt(a00[:,0]*a11[:,0])
        data['%d%d'%(ii,len(zbins)-1)]=np.c_[temp,r01[:,1]]
        if ii==0:
            data['%d%d'%(len(zbins)-1,len(zbins)-1)]=a11
    data['ell']=ell

    print('step 2: doing cross-correlation with kappa map...')
    print('Loading kappa map...')
    
    #Planck 2018 maps can be downloaded from the Planck Legacy data base
    kappamap=hp.read_map('planck2018_lensmap_equatorial_alm.fits')
    kappamask=hp.read_map('planck2018_lensmask_equatorial.fits')
    totmask=mask*kappamask
    for ii in range(len(zbins)):
        ell,c_gk,a_gg,a_kk,r_gk=cp.gal_powerspectrum_corr(galmap[ii]*totmask,kappamap*totmask,totmask,lmax=lmax,nell=10,type1=1,type2=2,nside=nside,print_out=False)
        data['clgk%d'%ii]=c_gk
    
    print('step 2: doing cross-correlation with temperature map...')
    print('Loading temperature map...')
    
    #Planck 2018 maps can be downloaded from the Planck Legacy data base
    cmbtmap=hp.read_map('planck2018_tmap_equatorial_alm.fits')
    cmbtmask=hp.read_map('planck2018_tmask_equatorial.fits')
    totmaskt=mask*cmbtmask
    for ii in range(len(zbins)):
        ell,c_gt,a_gg,a_tt,r_gt=cp.gal_powerspectrum_corr(galmap[ii]*totmaskt,cmbtmap*totmaskt,totmaskt,lmax=lmax,nell=10,type1=1,type2=2,nside=nside,print_out=False)
        data['clgt%d'%ii]=c_gt

    #Now apply pixel window to the relavent terms in data
    pixwin=hp.sphtfunc.pixwin(nside,lmax=lmax)
    use=np.in1d(np.arange(lmax+1),ell)
    pixwin=pixwin[use]**2
    for ii in range(len(zbins)):
        data['%d%d'%(ii,ii)]=data['%d%d'%(ii,ii)]/pixwin[:,None]
        data['clgk%d'%ii]=data['clgk%d'%ii]/pixwin[:,None]
        data['clgt%d'%ii]=data['clgt%d'%ii]/pixwin[:,None]

    #save data here
    iswf.dump_save(data,savedir+'auto_cross_data_pixwin-2018-lmax-%s.pkl'%lmax)

if skip_corr==1:
    data=iswf.dump_load(savedir+'auto_cross_data_pixwin-2018-lmax-%s.pkl'%lmax)

view_data=0
if view_data==1:
    #plot clgg
    plot_Cls.plot_cl(data,zbins,plot_theory=False,cltype='clgg',save=False)
    #plot clgk
    plot_Cls.plot_cl(data,zbins,plot_theory=False,cltype='clgk',save=False)
    #plot clgt
    plot_Cls.plot_cl(data,zbins,plot_theory=False,cltype='clgt',save=False)

    
#The option to compute theory
theory=0
#best-fit redshift distribution here, with the format of column: 0=sample in z; 1,2,3,4=p(z) in each redshift slice
nzbfitdir=savedir+'nz_3dinfer_param_Planck18_ell10_500-2bias-pixwin.minimizer.bfit'
lmax=500
#Cosmology already given in front
if theory==1:
    #theory
    print('Computing theory...')
    print('Loading power spectrum.')
    #input non-linear power spectrum generated from CAMB, with format: row 0 (excluding index 0): sampling in z; column 0 (excluding index 0): sample in k; [1:,1:] the power spectrum value at given z and k.
    fin=np.loadtxt('P_nonlinear-commonbins_Planck18-CAMB.txt')
    zpk=fin[0,1:]
    k=fin[1:,0]
    pknl=fin[1:,1:]*k[:,None]**3/2./np.pi**2
    #similiarly for the linear power spectrum
    fin=np.loadtxt('P_linear-commonbins_Planck18-CAMB.txt')
    pklin=fin[1:,1:]*k[:,None]**3/2./np.pi**2
    
    #loading nz
    print('Loading best-fit n(z).')
    fin=np.loadtxt(nzbfitdir)
    zz=fin[:,0]
    nz=fin[:,1:]
    #combined nz
    nztot=np.zeros(len(zz))
    for ii in range(len(zbins)-1):
        nztot+=nz[:,ii]*sum(galmap[ii])/sum(galmap[len(zbins)-1])
    #append to nz
    nz=np.c_[nz,nztot]

    #clgg
    print('Calculating 2-bias model for clgg...')
    ellth=np.arange(1,lmax+1)
    cl_nl=cp.cl_integral_ef(ellth,zz,nz,zpk,k,pknl,Omm=omega_m)
    cl_lin=cp.cl_integral_ef(ellth,zz,nz,zpk,k,pklin,Omm=omega_m)
    
    #fit bias
    def fit_2bias(bias,d1,d2lin,d2nl,sig):
        d2=d2lin*bias[0]**2+d2nl*bias[1]**2
        chi2=sum((d1-d2)**2/sig**2)
        return chi2
    
    def fit_1bias(d1,d2,sig):
        v1=d1/d2
        s1=sig/d2
        w=1/s1**2
        w=w/sum(w)
        b2=sum(w*v1)
        return np.sqrt(b2)

    blin=[]
    bnl=[]
    twobiasth=[]
    meanz=[]
    indmax=lmax/10
    #find b1 b2 for all entries but don't fit for the unbinned case
    for ii in range(len(zbins)):
        key='%d%d'%(ii,ii)
        usedata=data[key][:indmax,:]
        clout1=cp.theory_format(ellth,cl_lin[key],lmax=lmax,nell=10,fmt=1)
        clout2=cp.theory_format(ellth,cl_nl[key],lmax=lmax,nell=10,fmt=1)
        nl_only=clout2-clout1
        
        bias0=fit_1bias(usedata[1:30,0],clout2[1:30],usedata[1:30,1])
        p0=[bias0,bias0*1.1]
        res=minimize(fit_2bias,p0,args=(usedata[1:,0],clout1[1:],nl_only[1:],usedata[1:,1]))
        print('linear, non linear bias %s, chi square %s.'%(res.x,res.fun))
        
        if ii<len(zbins)-1:
            blin.append((res.x)[0])
            bnl.append((res.x)[1])
            theory=clout1*(res.x)[0]**2+nl_only*(res.x)[1]**2
            twobiasth.append(theory)
            
            #mean redshifts
            z_bar=sum(nz[:,ii]*zz)/sum(nz[:,ii])
            meanz.append(z_bar)

    #look at the bias evolution curve
    def bzf(z,param):
        poly_f=np.poly1d(param)
        bz=poly_f(z)
        minb=-param[1]/(2*param[0])
        ind=z<=minb
        if z[ind].size<=1:
            return bz
        elif z[ind].size>1:
            usemin=bz[ind][-1]
            bz[ind]=np.ones(len(bz[ind]))*usemin
        return bz    
    
    param=np.polyfit(meanz,blin,2)
    bzlin=bzf(zz,param)
    #in case p[0]<0, specify the function
    def fit_quaratic(param,x,y):
        q=param[0]*x**2+param[1]*x+param[2]
        return sum((q-y)**2)
    #param=np.polyfit(meanz,bnl,2)
    #bznl=bzf(zz,param)
    bnds=((0,None),(None,None),(None,None))
    res=minimize(fit_quaratic,param,args=(np.array(meanz),np.array(bnl)),bounds=bnds)
    print(res.x)
    bznl=bzf(zz,res.x)

    #####Or input your own b(z) here:
    #bzlin=np.loadtxt('bzlin_halfbin.txt')
    #bznl=np.loadtxt('bznl_halfbin.txt')

    if(1):
        pl.figure()
        pl.plot(meanz,blin,'o')
        pl.plot(meanz,bnl,'o')
        pl.plot(zz,bzlin)
        pl.plot(zz,bznl)
        pl.show()
   
    #iterative bias (delta_bias)
    print('Computing iterative redshift-dependent bias...')
    ib1,ib2=cp.iterative_bz(bzlin,bznl,data,lmax,ellth,zz,nz,zpk,k,pklin,pknl,omega_m,return_cl=False)
    #second iteration
    bzlin=bzlin[:,None]*ib1[None,:]
    bznl=bznl[:,None]*ib2[None,:]
    clgg_bz=cp.iterative_bz(bzlin,bznl,data,lmax,ellth,zz,nz,zpk,k,pklin,pknl,omega_m,return_cl=True)
     
    #save theory
    if(1):
       iswf.dump_save(clgg_bz,savedir+'clgg_bz-nz500.pkl')
    #look at the fitting here:
    if(1):
       plot_Cls.plot_cl(data,zbins,plot_theory=True,clth=clgg_bz,cltype='clgg',lmax=lmax) 
    
    #save the bz
    #np.savetxt(savedir+'bzlin.txt',bzlin)
    #np.savetxt(savedir+'bznl.txt',bznl)
    #exit()
     
    print('Computing theory for clgk...')
    clgkth_bz={}
    for ii in range(len(zbins)):
        clgk_nl=cp.clgk_integral(ellth,zz,nz[:,ii],zpk,k,pknl,Omm=omega_m,fitbz=True,bz=bznl[:,ii])
        clgk_lin_sub=cp.clgk_integral(ellth,zz,nz[:,ii],zpk,k,pklin,Omm=omega_m,fitbz=True,bz=bznl[:,ii])
        clgk_lin=cp.clgk_integral(ellth,zz,nz[:,ii],zpk,k,pklin,Omm=omega_m,fitbz=True,bz=bzlin[:,ii])
        temp=clgk_nl-clgk_lin_sub+clgk_lin
        clgkth_bz['clgk%d'%ii]=cp.theory_format(ellth,temp,lmax=lmax,nell=10,fmt=1)
    
    #save theory
    if(0):
        iswf.dump_save(clgkth_bz,savedir+'clgk_bz-2018.pkl')
    #look at the fitting here
    if(0):
        plot_Cls.plot_cl(data,zbins,plot_theory=True,clth=clgkth_bz,cltype='clgk',lmax=lmax)

    #Now look at constrants on Ak, and save the likelihood curves
    Ak=np.linspace(0.6,1.2,30)
    ellmax=np.arange(10,60,10)
    Ak_likelihood,Ak_likelihood_stats=cp.A_ellmax_multibins(Ak,ellmax,zbins,data,clgkth_bz,cltype='clgk')
    if(0):
        iswf.dump_save(Ak_likelihood,savedir+'Ak_likelihood.pkl')
        iswf.dump_save(Ak_likelihood_stats,savedir+'Ak_likelihood_stats.pkl')
    #show
    if(1):
        pl.figure()
        for ii in range(len(zbins)-1):
            key='clgk%d'%ii
            pl.errorbar(ellmax*10+ii*5,Ak_likelihood_stats[key][:,0],yerr=Ak_likelihood_stats[key][:,1],fmt='o',capsize=0)
        key='product'
        pl.errorbar(ellmax*10-5,Ak_likelihood_stats[key][:,0],yerr=Ak_likelihood_stats[key][:,1],fmt='o',capsize=0,color='k',fillstyle='none')
        key='unbin'
        pl.errorbar(ellmax*10-(ii+1)*5,Ak_likelihood_stats[key][:,0],yerr=Ak_likelihood_stats[key][:,1],fmt='o',capsize=0,color='k')
        pl.ylim([0.75,1.1])
        pl.ylabel('Ak')
        if(0):
            pl.savefig(savedir+'Ak_likelihood_stats.png',bbox_inches='tight')
        pl.show()

    #exit()
    
    print('Computing theory for clgt...')
    clgtth_bz={}
    for ii in range(len(zbins)):
        clgt_nl=cp.clgt_integral(ellth,zz,nz[:,ii],zpk,k,pknl,Omm=omega_m,fitbz=True,bz=bznl[:,ii])
        clgt_lin_sub=cp.clgt_integral(ellth,zz,nz[:,ii],zpk,k,pklin,Omm=omega_m,fitbz=True,bz=bznl[:,ii])
        clgt_lin=cp.clgt_integral(ellth,zz,nz[:,ii],zpk,k,pklin,Omm=omega_m,fitbz=True,bz=bzlin[:,ii])
        temp=clgt_nl-clgt_lin_sub+clgt_lin
        clgtth_bz['clgt%d'%ii]=cp.theory_format(ellth,temp,lmax=lmax,nell=10,fmt=1)

    #save theory
    if(0):
        iswf.dump_save(clgtth_bz,savedir+'clgt_bz-2018.pkl')
    #look at the fitting here
    if(0):
        plot_Cls.plot_cl(data,zbins,plot_theory=True,clth=clgtth_bz,cltype='clgt',lmax=lmax)
    
    At=np.linspace(-1,2,30)
    ellmax=np.arange(10,60,10)
    At_likelihood,At_likelihood_stats=cp.A_ellmax_multibins(At,ellmax,zbins,data,clgtth_bz,cltype='clgt')
    if(0):
        iswf.dump_save(At_likelihood,savedir+'At_likelihood.pkl')
        iswf.dump_save(At_likelihood_stats,savedir+'At_likelihood_stats.pkl')
    #show
    if(1):
        pl.figure()
        for ii  in range(len(zbins)-1):
            key='clgt%d'%ii
            pl.errorbar(ellmax*10+ii*5,At_likelihood_stats[key][:,0],yerr=At_likelihood_stats[key][:,1],fmt='o',capsize=0)
        key='product'
        pl.errorbar(ellmax*10-5,At_likelihood_stats[key][:,0],yerr=At_likelihood_stats[key][:,1],fmt='o',capsize=0,color='k',fillstyle='none')
        key='unbin'
        pl.errorbar(ellmax*10-(ii+1)*5,At_likelihood_stats[key][:,0],yerr=At_likelihood_stats[key][:,1],fmt='o',capsize=0,color='k')
        pl.ylabel('At')
        if(0):
            pl.savefig(savedir+'At_likelihood_stats.png',bbox_inches='tight')
        pl.show()

exit()
