#A simple python script that reads in omegam and sigma8
#and plots using chainconsumer [https://samreay.github.io/ChainConsumer/] is:

import numpy as np
import os
import sys

sys.path.append('/Library/Python/2.7/site-packages')

from chainconsumer import ChainConsumer

import matplotlib.pyplot as plt


def load_planck_chains(chainroot,plane_2d,nchain=4,rootdir=''):

    if(rootdir==''):
       if('mnu' in chainroot):
           rootdir='COM_CosmoParams_fullGrid_R3.01/base_mnu/CamSpecHM_TTTEEE_lowl_lowE/'
       else:
           rootdir='planck_chains/COM_CosmoParams_base-plikHM-TTTEEE-lowl-lowE_R3.00/base/'
           rootdir=rootdir+chainroot[5:]+'/'

    #get the colmap
    parfile=rootdir+chainroot+'.paramnames'
    col_map={}
    with open(parfile,'r') as fin:
        tlines=fin.readlines()
        for ii, tline in enumerate(tlines):
            tspl=tline.split()
            if(tspl[0]=='omegam*'):
                col_map['omega_m']=ii+2
            elif(tspl[0]=='sigma8*'):
                col_map['sigma8']=ii+2
            elif(tspl[0]=='mnu'):
                col_map['mnu']=ii+2

            #if(len(col_map.keys())==2):
            #            break



    col_map['weight']=0
    #col_map['weight']=1
    col_map['loglike']=1
    pardic={}
    for ww,walker in enumerate(np.arange(1,5)):
        chainfile=rootdir+chainroot+'_%d.txt'%walker
        chain_this=np.loadtxt(chainfile)

        for tt,tkey in enumerate(col_map.keys()):
            pv_this=chain_this[:,col_map[tkey]]
            if(ww==0):
               pardic[tkey]=pv_this.copy()
            else:
                pardic[tkey]=np.append(pardic[tkey],pv_this)


    if(plane_2d=='omS8'):
        S8=(pardic['omega_m']/0.3)**.5*pardic['sigma8']
        ch=np.column_stack([pardic['omega_m'],S8])
    elif(plane_2d=='omsigma8'):
        ch=np.column_stack([pardic['omega_m'],pardic['sigma8']])
    elif(plane_2d=='omsigma8mnu'):
        ch=np.column_stack([pardic['omega_m'],pardic['sigma8'],pardic['mnu']])

    
    if(True):#col_map['weight']==1):
        print('pre: ',pardic['weight'].min(),pardic['weight'].max())
        indmin=np.argmin(pardic['loglike'])
        print('min log like:',indmin,pardic['loglike'][indmin],ch[indmin,:])

    return ch,pardic['weight']

def load_DES_chains(chainfile,plane_2d):
    ch=[]; w=[];col_map={}

    with open(chainfile,'r') as fin:
        for ii, line in enumerate(fin):
            if(ii==0):
                #parse the parameters
                tspl=line.split()
                for cnum,col in enumerate(tspl):
                    cspl=col.split('--')
                    if(cspl[-1]=='omega_m'):
                        col_map['omega_m']=cnum
                    elif(cspl[-1]=='SIGMA_8'):
                        col_map['sigma8']=cnum

                    if(len(col_map.keys())==2):
                        break
                print(chainfile,'colum_map',col_map)

            if(line[0]=='#'):
                continue

            words=line.split()
            om=float(words[col_map['omega_m']])  
            sigma8=float(words[col_map['sigma8']]) 
            S8=(om/0.3)**.5*sigma8

            if(plane_2d=='omS8'):
                ch.append([om,s8])
            else:
                ch.append([om,sigma8])

            w.append(float(words[-1]))
    
    return np.array(ch), np.array(w)

def Asymm_Gaus(xin,mu=None,sig_pos=None,sig_neg=None):
    ''' Eveluate likelihood for asymmetric errors based on model 2 of 
    https://www.slac.stanford.edu/econf/C030908/papers/WEMT002.pdf
    xin: input array to evaluate likelihood,
    mu=mean of the distribution
    sig_pos: positive sigma
    sig_neg: negative error'''
  
    if(sig_neg==None or sig_pos==None):#Use gaussian
        if(sig_neg==None):
            sigma=sig_pos
        else:
            sigma=sig_neg

        weight=np.exp(-0.5*np.power((xin-mu)/sigma,2))
        weight=weight/(np.sqrt(2*np.pi)*sigma)
    else: #use model 2 for skewed gaussian
        sigma=0.5*(sig_pos+sig_neg)
        alpha=0.5*(sig_pos-sig_neg)
        A=(sig_pos-sig_neg)/(sig_pos+sig_neg)

        #Now convert xin to u
        uu=(np.sqrt(sigma*sigma + 4*alpha*(xin-mu-alpha)) -sigma )/ (2*alpha)
         

        #distribution is G(u)/|sigma+2*alpha*u
        weight=np.exp(-0.5*np.power(uu,2))
        weight=weight/(np.sqrt(2*np.pi))
        weight=weight/np.abs(sigma+2*alpha*uu)
        #get the imaginary uu
        ind_im=sigma*sigma + 4*alpha*(xin-mu-alpha)<0
        weight[ind_im]=0

    return weight 

def test_asymmetric():
    xin=np.linspace(0.35,0.47,200)
    mu=0.416; sig_pos=0.013;sig_neg=0.011
    #get a gaussian case
    wgaus_pos=Asymm_Gaus(xin,mu=mu,sig_pos=sig_pos,sig_neg=None)
    #get a gaussian case
    wgaus_neg=Asymm_Gaus(xin,mu=mu,sig_pos=None,sig_neg=sig_neg)

    #get Kids asymmetric case
    wasym=Asymm_Gaus(xin,mu=mu,sig_pos=sig_pos,sig_neg=sig_neg)

    #plot them
    plt.figure()
    plt.plot(xin,wgaus_pos,'r--',label='gaussian+')
    plt.plot(xin,wgaus_neg,'b--',label='gaussian-')
    plt.plot(xin,wasym,'k-',label='asym')

    #plot mu
    plt.plot([mu,mu],[0,wgaus_neg.max()],'k-.',label='mu=0.416')
    #plot +sig
    plt.plot([mu+sig_pos,mu+sig_pos],[0,wgaus_neg.max()],'r-.',label='mu=%5.3f+%5.3f'%(mu,sig_pos))
    #plot -sig
    plt.plot([mu-sig_neg,mu-sig_neg],[0,wgaus_neg.max()],'b-.',label='mu=%5.3f-%5.3f'%(mu,sig_neg))
    
    plt.ylabel('weight',fontsize=22)
    plt.xlabel('x',fontsize=22)
    plt.legend(fontsize=10)
    plt.tight_layout()

    plt.savefig('Asyymetric_error.png')
    plt.show()
    sys.exit()
            
    return


def load_Legacy(comb=np.array([])):
    #'base_plikHM_TTTEEE_lowl_lowE'
    om_fid_planck=0.3209
    sigma8_fid_planck=0.819
    om_sigma8_fid=0.9*om_fid_planck*sigma8_fid_planck

    if(comb.size==0):
       om_this=np.linspace(0.20,0.48,120)
       sig8_this=np.linspace(0.4,1.2,120)
       om,sig8=np.meshgrid(om_this,sig8_this)
    else:
        om=comb[:,0]
        sig8=comb[:,1]


    om_1d=om.reshape(om.size)
    sig8_1d=sig8.reshape(sig8.size)
    omsig=om_1d*sig8_1d
    weight=np.exp(-0.5*np.power((omsig-om_sigma8_fid)/(0.03*om_sigma8_fid),2))
    weight=weight/(np.sqrt(2*np.pi)*0.03*om_sigma8_fid)
    
    if(comb.size!=0):
       weight=weight*comb[:,2]

    return np.column_stack([om_1d,sig8_1d]),weight

def fnu_prior(mnu_1sig=0.268,om_m_fid=0.3209,fnu_g=np.linspace(0,0.03,30)):
    '''provides the prior for neutrino mass'''
    fnu_1sig=(mnu_1sig/93.14)/om_m_fid
    
    fnu_max=max(fnu_g.max(),5*fnu_1sig)
    fnu_1d=np.linspace(0,fnu_max,fnu_max/0.001)

    Prob=np.exp(-0.5*np.power((fnu_1d-0)/fnu_1sig,2))
    Prob=Prob/np.sum(Prob*(fnu_1d[1]-fnu_1d[0])) #need proper normalization

    weight=np.interp(fnu_g,fnu_1d,Prob)

    return weight

def plot_om_s8_power_law(product=0.9,error=0.03,om_pow=0.8,error_neg=None,colin='b',
        om_this=np.linspace(0.20,0.40,200),sig8_this=np.linspace(0.54,1.1,200),like=False,marg_fnu=False):
    
    #om_this=np.linspace(0.20,0.40,100)
    sigma8_this=product/np.power(om_this,om_pow)
    sigma8_1low=(product-error)/np.power(om_this,om_pow)
    sigma8_1hig=(product+error)/np.power(om_this,om_pow)

    sigma8_2low=(product-2*error)/np.power(om_this,om_pow)
    sigma8_2hig=(product+2*error)/np.power(om_this,om_pow)

    if(like==True):
        #om_this=np.linspace(0.20,0.40,200)
        #sig8_this=np.linspace(0.54,1.1,200)
        fnu_this=np.linspace(0,0.04,40)
        if(marg_fnu==False):
            om,sig8=np.meshgrid(om_this,sig8_this)
    
            om_1d=om.reshape(om.size)
            sig8_1d=sig8.reshape(sig8.size)
            omsig=np.power(om_1d,om_pow)*sig8_1d
            #weight=np.exp(-0.5*np.power((omsig-product)/error,2))
            #weight=weight/(np.sqrt(2*np.pi)*error)
            weight=Asymm_Gaus(omsig,mu=product,sig_pos=error,sig_neg=error_neg)
            return np.column_stack([om_1d,sig8_1d,weight])
        elif(marg_fnu==True):
            om_g,sig8_g,fnu_g=np.meshgrid(om_this,sig8_this,fnu_this)

            om_1d=om_g.reshape(om_g.size)
            sig8_1d=sig8_g.reshape(sig8_g.size)
            fnu_1d=fnu_g.reshape(fnu_g.size)
            omsig=np.power(om_1d,om_pow)*sig8_1d*(1-4*fnu_1d)
            weight=np.exp(-0.5*np.power((omsig-product)/error,2))
            weight=weight/(np.sqrt(2*np.pi)*error)
            
            #neutrino mass prior
            wmnu=fnu_prior(mnu_1sig=0.268/2,om_m_fid=0.3209,fnu_g=fnu_1d)
            weight=weight*wmnu

            return np.column_stack([om_1d,sig8_1d,fnu_1d,weight])
    else:
        #plot our contraints
        #plt.plot(om_this,sigma8_this,'b-',lw=2,label=r'$\Omega_m \sigma_8=0.9*%4.2f \pm 3$'%(om_sigma8_fid))
        plt.plot(om_this,sigma8_1low,colin+'--',lw=1);plt.plot(om_this,sigma8_1hig,colin+'--',lw=1)
        plt.plot(om_this,sigma8_2low,colin+'--',lw=1);plt.plot(om_this,sigma8_2hig,colin+'--',lw=1)
        plt.fill_between(om_this,sigma8_1low,sigma8_1hig,color=colin,alpha=0.4)
        plt.fill_between(om_this,sigma8_2low,sigma8_2hig,color=colin,alpha=0.2)

  
    return 

def Legacy_Ak(ompow=0.75):
    '''returns the legacy lensing results'''
    om_fid_planck=0.315 #confirmed by Ellen on 19 Auagust 0.3209
    sigma8_fid_planck=0.811 #confirmed by ellen on 19 August 0.819
    om_sigma8_fid=0.90*np.power(om_fid_planck,ompow)*sigma8_fid_planck
    #om_sigma8_fid=0.87*np.power(om_fid_planck,ompow)*sigma8_fid_planck


    product=om_sigma8_fid
    error=(0.027/0.90)*om_sigma8_fid

    return product,error

def Lensing_likes(like=False,fnu=0):

    #grid variable
    om_this=np.linspace(0.20,0.40,200)
    sig8_this=np.linspace(0.60,1.1,200)

    #legacy survey
    product,error=Legacy_Ak(ompow=0.785)
    #accounting for the effect of neutrino 
    product=product/np.power(1.0-(4*fnu),1.0)
    #To plot the Legacy survey result with 3% error
    legacy_like=plot_om_s8_power_law(product=product,error=error,om_pow=0.785,
            om_this=om_this,sig8_this=sig8_this,colin='b',like=like)
    
    #planck lensing constraint   0.589 +/- 0.020 
    cmb_like=plot_om_s8_power_law(product=0.589,error=0.02,om_pow=0.25,om_this=om_this,sig8_this=sig8_this,colin='m',like=like)
    if(True):
        S8=np.power(cmb_like[:,0],0.5)*cmb_like[:,1]
        S8_mu=np.average(S8,weights=cmb_like[:,2])
        S8_mu2=np.average(S8*S8,weights=cmb_like[:,2])
        S8_err=S8_mu2-np.power(S8_mu,2)
        print('cmb S8:',S8_mu,S8_err)

    #kids 1000
    kids_value=0.759*np.power(0.3,0.5)
    kids_error=0.024*np.power(0.3,0.5)
    kids_neg_error=0.021*np.power(0.3,0.5)
    kids_like=plot_om_s8_power_law(product=kids_value,error=kids_error,om_pow=0.5,error_neg=kids_neg_error,
                       om_this=om_this,sig8_this=sig8_this,colin='g',like=like)

    #DES Y1 (cosmic shear: from https://arxiv.org/pdf/1708.01530.pdf table II on page 16)
    DES_value=0.782*np.power(0.3,0.5)
    DES_error=0.027*np.power(0.3,0.5)
    DES_like=plot_om_s8_power_law(product=DES_value,error=DES_error,om_pow=0.5,om_this=om_this,sig8_this=sig8_this,colin='y',like=like)


    #interpolate the planck likelihood
    #planck_like=interpolate_planck(om_this,sig8_this)
    planck_like=planck_2d_gaus_grid(om_this,sig8_this)

    if(like==True):
        like_dic={'oms8':legacy_like[:,:2],
                'legacy':legacy_like[:,2],
                'cmb':cmb_like[:,2],
                'kids':kids_like[:,2],
                'des':DES_like[:,2],
                'planck':planck_like
                }
    else:
        like_dic={}

    return like_dic

def plot_mnu_track(conf):
    '''This adds a track of mnu'''
  
    mnu_split=[0,0.06,0.1,0.15,0.25]
    mnu_alpha=[0.1,0.3,0.4,0.6]

    ch,w=load_planck_chains('base_mnu_CamSpecHM_TTTEEE_lowl_lowE','omsigma8mnu')
  
     
    print(ch[:,2].min(),ch[:,2].max(),ch.shape)
    for ii in range(0,len(mnu_split)-1):
       #ch,w=load_planck_chains('base_mnu_CamSpecHM_TTTEEE_lowl_lowE','omsigma8mnu')
       indsel=(ch[:,2]>mnu_split[ii])*(ch[:,2]<mnu_split[ii+1])
       cname=r'$%4.2f<\Sigma m_{\nu}<%4.3f$'%(mnu_split[ii],mnu_split[ii+1])
       print(ii,cname,np.sum(indsel))

       conf.add_chain(ch[indsel,:2],parameters=names,weights=w[indsel],name=cname,grid=False)
       
       chain_prop={'color':'y','shade':True,'shade_alpha':mnu_alpha[ii],'ls':'--','lw':1.5}

       for tt,tkey in enumerate(plot_prop.keys()):
           plot_prop[tkey].append(chain_prop[tkey])

    return

def print_pardetails(ch,w,chainfile):
    '''This print some parameter related details'''

    #find the best fit
    indbfit=np.argmax(w)
    wbfit=w[indbfit]
    bfit_par=ch[indbfit,:]
    loglike_bfit=-2.0*np.log(w[indbfit]/wbfit)

    #find the w at 0m=0.295 and sigma8=0.8
    for ref in [0,1,2]:
        if(ref==0):
            #om_ref=0.295;sig8_ref=0.80
            #om_ref=0.293;sig8_ref=0.796
            om_ref=0.30;sig8_ref=0.80
        elif(ref==1):
            #om_ref=0.3209;sig8_ref=0.819
            om_ref=0.316;sig8_ref=0.812
        elif(ref==2):
            om_ref=0.286;sig8_ref=0.796

        dist=np.sqrt(np.power(ch[:,0]-om_ref,2)+np.power(ch[:,1]-sig8_ref,2))
        indref=np.argmin(dist)
        ref_par=ch[indref,:]
        loglike_ref=-2.0*np.log(w[indref]/wbfit)

        print(ref,chainfile,'bfit: ',bfit_par,loglike_bfit, 'ref: ',ref_par,loglike_ref, 'delta Chi2:',loglike_ref-loglike_bfit)

    return

def interpolate_planck(om1d,s81d):
    #load planck chain and interpolate to the grid
    ch,w=load_planck_chains('base_plikHM_TTTEEE_lowl_lowE',plane_2d)

    #make grid with shift by 0.5 bin
    dom=om1d[1]-om1d[0]
    ds8=s81d[1]-s81d[0]
    om1d_grid=np.append(om1d-0.5*dom,om1d[-1]+0.5*dom)
    s81d_grid=np.append(s81d-0.5*ds8,s81d[-1]+0.5*ds8)

    H,xed,yed=np.histogram2d(ch[:,0],ch[:,1],bins=[om1d_grid,s81d_grid],weights=w)
    
    H=H/(np.sum(H)*dom*ds8)

    return H.T.reshape(H.size)


def planck_2d_gaus_grid(om1d,s81d):
    '''fits a 2d guassian and then interpolate to a grid'''
    ch,w=load_planck_chains('base_plikHM_TTTEEE_lowl_lowE',plane_2d)

    #get the mean
    mu_2d=np.average(ch,weights=w,axis=0)

    cov_2d=np.cov(ch.T)#,weights=w)
    icov_2d=np.linalg.inv(cov_2d)
    print('mu cov icov',mu_2d,cov_2d),icov_2d

    om_2d,sig8_2d=np.meshgrid(om1d,s81d)
   
    
    #generate 2d gaussian
    chi2=np.zeros(om_2d.size)
    for ii in range(0,chi2.size):
        delta=np.array([om_2d.flatten()[ii]-mu_2d[0],sig8_2d.flatten()[ii]-mu_2d[1]])
        chi2[ii]=np.dot(delta,np.dot(icov_2d,delta.T))
    
    chi2_2d=chi2.reshape(om_2d.shape)
    wgaus=np.exp(-chi2_2d/2)
    wgaus=wgaus/wgaus.sum()

    #from scipy.stats import multivariate_normal
    #dist_2d = multivariate_normal(mean=[0,0], cov=[[1,0],[0,1]])
   
    return wgaus.flatten()

#sigma8_this=0.9*om_sigma8_fid/om_this
#sigma8_1low=0.9*om_sigma8_fid*(1.0-0.03)/om_this
#sigma8_1hig=0.9*om_sigma8_fid*(1.0+0.03)/om_this

#To test the asymmetric error handling
#test_asymmetric()

c = ChainConsumer()

plane_2d='omsigma8'
#plane_2d='om_mnu'
#plane_2d='omS8'

if(plane_2d=='omS8'):
    names=['$\\Omega_m$','$S_8$']
else:
    names=['$\\Omega_m$','$\sigma_8$']


chains_dic={'DES':['DES_chains/d_l3.txt'],#'DES_chains/dpnl_l3.txt'],
            'DES-names':[r'DES Y1(3X2)'],#,'DES Y1(3x2)+Planck'],
           
            #'planck':['base_plikHM_TTTEEE_lowl_lowE',
                #'base_mnu_CamSpecHM_TTTEEE_lowl_lowE'
                #'base_plikHM_TTTEEE_lowl_lowE_lensing'
            #    ],
            #'planck-names':[r'{\it Planck} 2018 (TTTEEE + lowE)',
                #'Planck 2018 (no lensing, wmnu)'
                #'Planck 2018'
            #    ],
            
            'BOSS':['BOSSDR12_only'],
            'BOSS-names':['BOSS LRG (DR12)'],

            'kids':['kids2dflensfiducial'],
            'kids-names':['KiDS-450 (3X2)'],

            'This':['base_plikHM_TTTEEE_lowl_lowE_lensing'],
            'This-names':['Planck+Legacy Survey'],

            'lensing1':['cmb','des+kids','legacy','des+kids+legacy+cmb'],#'kids','des','legacy'],#,'legacy+cmb+kids'],
            'lensing1-names':['CMB lensing',r'KiDS 1000 + DES Y1 ($\xi_{+/-}$)','Legacy Survey (This work)','All lensing'],#'Legacy Survey (This work)','Lensing (Legacy+Kids+CMB)'],

            'lensing2':['planck','planck+des+kids+legacy+cmb'],
            'lensing2-names':[r'{\it Planck} 2018 (TTTEEE + lowE)','All lensing + {\it Planck} 2018'],

            'lensing_fnu':['legacy'],#,'legacy_fnu',],
            'lensing_fnu-names':['Legacy Survey (This work)','Legacy Survey+fnu prior'],
            }


fnu=0.00
#First plot the lensing likelihood
like_dic=Lensing_likes(like=True,fnu=fnu)

marg_fnu=False


chain_name=[r'DES Y1(3X2)','Planck','DES Y1(3x2)+Planck']

plot_prop={'color':[],'shade':[],'shade_alpha':[],'ls':[],'lw':[]}


for ss,survey in enumerate(['lensing1','lensing2']):#,'lensing_fnu']):
    if('-names' in survey):
        continue
    for ff,chainfile in enumerate(chains_dic[survey]):
        if(survey=='DES'):
            ch,w=load_DES_chains(chainfile,plane_2d)
            grid=False
            chain_prop={'color':'y','shade':True,'shade_alpha':1.0,'ls':'-','lw':1.2}
        elif(survey=='planck'):
            ch,w=load_planck_chains(chainfile,plane_2d)
            grid=False
            chain_prop={'color':'#61B329',#'#90FEFB',#'#49E9BD',
                    'shade':True,'shade_alpha':0.8,'ls':'-','lw':1.5}
        elif(survey=='BOSS'):
            ch,w=load_planck_chains(chainfile,plane_2d,nchain=16,rootdir='BOSSDR12chains/')
            grid=False
            chain_prop={'color':'black','shade':False,'shade_alpha':2.0,'ls':'--','lw':1.2}
        elif(survey=='kids'):
            ch,w=load_planck_chains(chainfile,plane_2d,nchain=8,rootdir='kids2dflenschains/')
            grid=False
        elif(survey=='This'):
            ch,w=load_planck_chains(chainfile,plane_2d)
            #reweight for us
            ch,w=load_Legacy(comb=np.column_stack([ch,w]))
            grid=False
        elif(survey in ['lensing1','lensing2']):
            ch=like_dic['oms8']
            w=np.ones(ch.shape[0])
            ls='-';alpha=0.4;shade=True;lw=1.2
            for lll in ['legacy','cmb','kids','des','planck']:
                if(lll in chainfile):
                   w=w*like_dic[lll]
            grid=True
            lens_col_dic={'cmb':'orange','kids':'magenta','des':'orange','des+kids':'purple',
                    'legacy':'red',#'#49E9BD',
                    'planck':'#61B329',
                    'des+kids+legacy+cmb':'blue','planck+des+kids+legacy+cmb':'k',
                    'cmb+legacy':'green'}

            if(chainfile=='des+kids+legacy+cmb'):
                alpha=0.75;lw=2.0;shade=True;
            elif(chainfile=='legacy'):
                alpha=0.7
            elif(chainfile=='cmb'):
                alpha=0.2
            elif(chainfile=='planck'):
                alpha=0.7
            elif(chainfile=='des'):
                alpha=0.2

            chain_prop={'color':lens_col_dic[chainfile],'shade':shade,'shade_alpha':alpha,'ls':ls,'lw':lw}
        elif(survey=='lensing_fnu'):
            product,error=Legacy_Ak(ompow=0.75)
            #accounting for the effect of neutrino 
            if(marg_fnu==False and chainfile!='legacy'):
                continue
            elif(marg_fnu==True and chainfile!='legacy_fnu'):
                continue
            elif(marg_fnu==False):
                product=product/np.power(1.0-(6*fnu),1.0)
                error=error/np.power(1.0-(6*fnu),1.0)
               
            like_this=plot_om_s8_power_law(product=product,error=error,om_pow=0.75,colin='b',like=True,marg_fnu=marg_fnu)
            ch=like_this[:,:2]; 
            if(marg_fnu==True):
                w=like_this[:,3]
            else:
                w=like_this[:,2]
            grid=True
            chain_prop={'color':'blue','shade':True,'shade_alpha':0.6,'ls':'--','lw':1.5}
       

        print_pardetails(ch,w,chainfile)

        c.add_chain(ch,parameters=names,weights=w,name=chains_dic[survey+'-names'][ff],grid=grid)
        for tt,tkey in enumerate(plot_prop.keys()):
            plot_prop[tkey].append(chain_prop[tkey])



#plot_mnu_track(c)

## Then after adding 2 more chains
c.configure(plot_hists=False,sigma2d=False,kde=1.5,max_ticks=10,#contour_labels="confidence", 
        colors=plot_prop['color'], linewidths=plot_prop['lw'], 
	legend_kwargs={"loc": "upper right", "fontsize": 10},
	legend_color_text=True, legend_location=(-1, 0),
        diagonal_tick_labels=False,shade=plot_prop['shade'],
        shade_alpha=plot_prop['shade_alpha'],linestyles=plot_prop['ls'])


table = c.analysis.get_latex_table(caption="Results for the tested models", label="tab:example")
print(table)


fig = c.plotter.plot(legend='t',figsize="column",extents=[[0.20,0.40],[0.6,1.0]])


#Lensing_likes(like=False,fnu=fnu)
plt.plot(0.296,0.798,'wx')

if(marg_fnu==True):
    nutag='_fnumarg'
else:
    nutag='_fnu%4.3f'%(fnu)
    nutag=''


#foutname='%s%s_eBOSS.pdf'%(plane_2d,nutag)
foutname='%s%s.pdf'%(plane_2d,nutag)
fig.savefig(foutname, bbox_inches="tight", 
	dpi=300, transparent=True, pad_inches=0.05)

print('created file: ',foutname)
plt.show()
