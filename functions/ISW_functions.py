import numpy as np
import scipy
from scipy import spatial
import healpy as hp
import pylab as pl
from astropy.io import fits
import pickle


def dump_save(stuff,filename):
    '''This saves the dictionary and loads it from appropriate files'''
    with open(filename,'wb') as fout:
        pickle.dump(stuff,fout,pickle.HIGHEST_PROTOCOL)
        #json.dump(self.impute, fout, sort_keys=True, indent=3)
    print('written impute ditionary:',filename)
    return 0
def dump_load(filename):
    with open(filename,'rb') as fin:
        stuff=pickle.load(fin)
        #self.impute = json.load(fin)
    #print('loaded impute ditionary:',filename)
    return stuff

#heapix functions


#rotational matrix
def rot_x(ang,deg=True):
    if deg==True:
        ang=ang*np.pi/180.
    return np.array([[1,0,0],[0,np.cos(ang),-np.sin(ang)],[0,np.sin(ang),np.cos(ang)]])

def rot_y(ang,deg=True):
    if deg==True:
        ang=ang*np.pi/180.
    return np.array([[np.cos(ang),0,np.sin(ang)],[0,1,0],[-np.sin(ang),0,np.cos(ang)]])

def rot_z(ang,deg=True):
    if deg==True:
        ang=ang*np.pi/180.
    return np.array([[np.cos(ang),-np.sin(ang),0],[np.sin(ang),np.cos(ang),0],[0,0,1]])

def rotate_map(hmap, rot_theta, rot_phi, nest=True, rand_xrot=False):
    """
    Take hmap (a healpix map array) and return another healpix map array 
    which is ordered such that it has been rotated in (theta, phi) by the 
    amounts given.
    """
    nside = hp.npix2nside(len(hmap))

    # Get theta, phi for non-rotated map
    #t,p = hp.pix2ang(nside, np.arange(hp.nside2npix(nside)), nest=True) #theta, phi
    xx,yy,zz = hp.pix2vec(nside, np.arange(hp.nside2npix(nside)), nest=nest)
    
    # Define a rotator
    #r = hp.rotator.Rotator(deg=True, rot=[rot_phi,rot_theta], inv=False, eulertype='X')
    rotmat=np.dot(rot_y(rot_theta),rot_z(-rot_phi))
    
    if rand_xrot==True:
        rand_ang=np.random.uniform(low=0,high=360,size=1)
        rotmat=np.dot(rot_x(rand_ang),rotmat)
    
    rot_xx,rot_yy,rot_zz=hp.rotator.rotateVector(rotmat,xx,yy,zz)
    
    # Get theta, phi under rotated co-ordinates
    #trot, prot = r(t,p)
    #rot_vector=r(vector)
    
    #prot, trot=hp.vec2ang(np.c_[rot_xx,rot_yy,rot_zz], lonlat=True)
    #rotpix=vec2pix(nside=8,xrot,yrot,zrot,nest=True)
    
    rot_pix=hp.vec2pix(nside,rot_xx,rot_yy,rot_zz,nest=nest)
    
    # Interpolate map onto these co-ordinates
    #rot_map = hp.get_interp_val(hmap, trot, prot,lonlat=True)
    rot_map=np.zeros(len(hmap))
    #assert(len(np.unique(rot_pix))==len(rot_pix))
    rot_map[rot_pix]=hmap

    return rot_map

def hp_pixelize(ra,dec,nside=256,nest=True,plots=0,title='',return_pix=False,limit=None):
    #number of pixel and are per pixel for given nside 
    npixel=12*np.power(nside,2)
    area_perpix=4.0*np.pi*np.power(180.0/np.pi,2)/npixel
    
    #find pixel for each entry
    pixel= hp.ang2pix(nside, ra,dec,nest=nest,lonlat=True)
    
    hbin=np.linspace(0,npixel,npixel+1)-0.5
    pixel_map,bb=np.histogram(pixel,bins=hbin)

    if(plots==1):
        pl.figure()
        hp.mollview(pixel_map,nest=nest,min=limit[0],max=limit[1])
        pl.title(title)
    if return_pix==False:   
        return pixel_map#,pixel
    elif return_pix==True:
        return pixel_map,pixel


def change_hpmap_coordinate(hpmap,nside,from_coord,to_coord,nest=False):
    
    r=hp.Rotator(coord=[to_coord,from_coord])
    #convert pixel map to angles
    ipix=np.arange(hpmap.shape[-1])
    
    Gangles=hp.pix2ang(nside, ipix, nest=nest, lonlat=False)
    
    Cangles=r(*Gangles, lonlat=False)
    
    new_pix = hp.ang2pix(nside, *Cangles)
    
    newmap=hpmap[..., new_pix]

    return newmap

def hp_pixelize_intensity(ra,dec,delta,nside=256,nest=True,plots=0,title='',normalize=False):
    #number of pixel and are per pixel for given nside 
    npixel=12*np.power(nside,2)
    area_perpix=4.0*np.pi*np.power(180.0/np.pi,2)/npixel
    
    #find pixel for each entry
    pixel= hp.ang2pix(nside, ra,dec,nest=nest,lonlat=True)
    
    hbin=np.linspace(0,npixel,npixel+1)-0.5
    pixel_map,bb=np.histogram(pixel,bins=hbin)
    
    pixel_intensity=np.zeros(npixel)
    for kk in range(len(pixel)):
        ind=pixel[kk]
        pixel_intensity[ind]+=delta[kk]
    
    ##if pixel_map=0 at any pixel, then intensity would also be zero
    zeroind=pixel_map==0
    pixel_map_non0=np.copy(pixel_map)
    pixel_map_non0[zeroind]=1.
    
    if normalize==True:
        pixel_intensity=pixel_intensity/pixel_map_non0.astype(float)

    if(plots==1):
        pl.figure()
        hp.mollview(pixel_map,nest=nest)
        pl.title(title)
        
        pl.figure()
        #hp.mollview(pixel_intensity_norm,nest=nest)
        hp.mollview(pixel_intensity,nest=nest)
        #pl.title(title)
        
    return pixel_map, pixel_intensity#, pixel_intensity_norm


def inverse_ra(ra):
    inv_ra=np.copy(ra)
    inv_ind=ra>180
    inv_ra[inv_ind]=inv_ra[inv_ind]-360
    return inv_ra
