"""
Here are some useful functions to do patch splits, (RA,Dec,Z) to Cartesian coordinates etc
"""

import numpy as np
import pylab as pl
import sys
sys.path.insert(0, '/disk1/qhang/Projects/CorrFunc_Shadab/')
import mycosmo

def patch_split(coord_gal, i):
    if i == 1:
        index1 = np.where((coord_gal[:,0]<141.)&(coord_gal[:,0]>129.))
        index11 = np.where((coord_gal[:,1]<3.)&(coord_gal[:,1]>-2.))
        index_1 = np.intersect1d(index1, index11)
        return coord_gal[index_1]
    if i == 2:
        index2 = np.where((coord_gal[:,0]<186.)&(coord_gal[:,0]>174.))
        index22 = np.where((coord_gal[:,1]<2.)&(coord_gal[:,1]>-3.))
        index_2 = np.intersect1d(index2, index22)
        return coord_gal[index_2]
    if i == 3:
        index3 = np.where((coord_gal[:,0]>211.5)&(coord_gal[:,0]<223.5))
        index33 = np.where((coord_gal[:,1]<3.)&(coord_gal[:,1]>-2.))
        index_3 = np.intersect1d(index3, index33)
        return coord_gal[index_3]
    else:
        print('input integer 1, 2, or 3')
        

def LtoMass(L):        
    Mp = 10**13.4 #h^{-1}M_sun
    L0 = 2.0e11 #h^{-2}L_sun
    alpha = 1.09   
    M = Mp*(L/L0)**alpha
    return M
        
        
def redshift_filter(Max, Min, color):
    index = np.where((color[:,2]>=Min)&(color[:,2]<=Max))
    color_filtered = color[index]
    return color_filtered


def z_distance(z,om_m=0.27):
    d = 2997.92458*z/(1+0.75*om_m*1.52*z+(om_m**0.4*z/2)**1.52)**(1./1.52)
    return d

def distance2z(distance,om_m=0.27,func=1):
    zz=np.linspace(0.,2.,5000)
    if func==1:
        dd=z_distance(zz,om_m=om_m)
    elif func==2:
        dd=np.zeros(len(zz))
        for ii in range(len(zz)):
            dd[ii]=mycosmo.r_comoving(zz[ii],100,om_m,1-om_m)
    zout=np.interp(distance,dd,zz)
    if(0):
        pl.plot(zz,dd,'.-')
        pl.plot(zout,distance,'o')
        #pl.xlim([zout-0.1,zout+0.1])
    return zout

def cartesian_convert(color):
    theta = color[:,1]*np.pi/180.0 #in radian          
    R = z_distance(color[:,2])
    phi = color[:,0]*np.pi/180.0      
    
    x = R*np.cos(theta)*np.cos(-phi)
    y = R*np.cos(theta)*np.sin(-phi)
    z = R*np.sin(theta)
    
    color_cartesian = np.column_stack([x,y,z])
    
    return color_cartesian

def xi2fullxi(xiT, neg=True):
    if neg!=True:
        xi_1q = xiT
        xi_2q = np.fliplr(xiT)
        xi_4q = np.flipud(xiT)
        xi_3q = np.fliplr(np.flipud(xiT))

        a = np.concatenate((xi_2q, xi_1q), axis = 1)
        b = np.concatenate((xi_3q, xi_4q), axis = 1)

        full_xi = np.concatenate((b, a), axis = 0)
    else:
        xi_1q = xiT
        xi_2q = np.fliplr(xiT)
        
        full_xi = np.concatenate((xi_2q, xi_1q),axis=1)
    
    return full_xi

def nice_plot_xi(xi, levels=[0.2,0.5,1,5], contour=True, c='w',colorbar=True, xlabel=True, ylabel=True, cmap='GnBu',cblabel=''):
    pl.rcParams.update({'font.size': 12})
    im = pl.imshow(np.log10(abs(xi)+10**(-8)), vmax=1, vmin=-2, extent=[-39.5,39.5,-39.5,39.5], origin='lower',cmap=cmap)
    if contour==True:
        CS = pl.contour(xi, levels = levels, extent=[-39.5,39.5,-39.5,39.5], colors=c)
        pl.clabel(CS, inline=0, fontsize=12)
    if colorbar==True:
        cbar=pl.colorbar(im,label=cblabel)
    if xlabel==True:
        pl.xlabel('$R_p\, [h^{-1}$Mpc]')
    if ylabel==True:    
        pl.ylabel('$\pi\, [h^{-1}$Mpc]')
    return 0
    
def corr_mat(cov):
    corr=np.zeros(cov.shape)
    for ii in range(0,cov.shape[0]):
        for jj in range(0,cov.shape[1]):
            corr[ii,jj]=cov[ii,jj]/np.sqrt(cov[ii,ii]*cov[jj,jj])
            
    return corr   

def helio_cmb_converter(ra, dec, z, to_frame):
    v_lg = 627.
    #Galactic coordinates
    ra_0 = 166.6
    dec_0 = -27.3
    c = 3.0E8
 
    z_dip_n = v_lg/c*(ra*ra_0+np.sin(ra/180.*np.pi)*np.sin(ra_0/180.*np.pi)*dec*dec_0)
    norm = (np.sqrt(ra_0**2+np.sin(ra_0/180.*np.pi)**2*dec_0**2)*np.sqrt(ra**2+np.sin(ra/180.*np.pi)**2*dec**2))
    z_dip=z_dip_n/norm
    
    if to_frame == 'cmb':
        z_new = (1+z)/(1+z_dip)-1
        #z_new=z+z_dip
    elif to_frame == 'heliocentric':
        z_new = (1+z)*(1+z_dip)-1
    
    return z_new

def flattenmodel(ximodel,rperarr,rpararr):
    xiquater = (ximodel.T)[:len(rperarr),len(rpararr):]
    xiflip = np.flipud(xiquater)
    xiflat = xiflip.flatten()
    return xiflat


def wpxi024_model(ximodel, rrarr=np.linspace(1,30,25), rperarr = np.linspace(0.5, 39.5, 40), rpararr = np.linspace(0.5, 39.5, 40)):
    #File input a directory
    #Model input full xi
    import sys
    sys.path.insert(0, '/disk1/qhang/CorrFuncShadab/')
    import General_function_HOD as GFHOD
    
    #rperarr = np.linspace(0.5, 39.5, 40)
    #rpararr = np.copy(rperarr)
    wp2d = flattenmodel(ximodel, rperarr,rpararr)
        
    njn=0
    #rrarr=np.linspace(1,30,25)
    thetaarr=np.linspace(0,0.5*np.pi,101)
    muarr=np.cos(thetaarr)

    #define the mapping between rp_rpi and r mu
    Irmu_dic=GFHOD.rmuweight_mapping(rperarr,rpararr,rrarr,muarr,mode='lin')
    
    wp_xy=wp2d.reshape(rperarr.size,rpararr.size)
    #get the projected correlation function by integrating along los
    wp1d=np.sum(wp_xy, axis=1)
    wp = np.column_stack((rperarr, wp1d*2))

    #compute xi in s mu space
    xismu=GFHOD.Interpolate_xi(wp_xy,Irmu_dic)

    #get the legendre polynomial from interpolated xismu
    xi024=GFHOD.Xi_Legendre('jnk',rrarr, thetaarr, xismu,samp='rtheta',write=0)
    xi024[:,1:]=2*xi024[:,1:]
    return wp, xi024


def xi024_file(wp2d, output,rrarr=np.linspace(1,30,25)):
    import sys
    sys.path.insert(0, '/disk1/qhang/CorrFuncShadab/')
    import General_function_HOD as GFHOD

    wp2dfile=wp2d
    #wpfile='/disk2/qhang/CovarianceCompare/Data/WP/Galauto_data-wp-rp-pi-NJN-54.txt'
    outputfilename=output

    rperarr,rpararr,wp2d= GFHOD.read_wp2d(wp2dfile)

    njn=wp2d.shape[1]-3

    #rrarr=np.linspace(1,30,25)
    thetaarr=np.linspace(0,np.pi,101)
    muarr=np.cos(thetaarr)

    #define the mapping between rp_rpi and r mu
    Irmu_dic=GFHOD.rmuweight_mapping(rperarr,rpararr,rrarr,muarr,mode='lin')


    #def 

    for ii in range(-1,njn): #first will the all one, rest will be jacknife realization
        #reshape the wp 2d rpeoperly to be used
        wp_xy=wp2d[:,ii+3].reshape(rperarr.size,rpararr.size)
        #get the projected correlation function by integrating along los
        wp1d=np.sum(wp_xy,axis=1)

        #compute xi in s mu space
        xismu=GFHOD.Interpolate_xi(wp_xy,Irmu_dic)

        #get the legendre polynomial from interpolated xismu
        xi024=GFHOD.Xi_Legendre('jnk',rrarr, thetaarr, xismu,samp='rtheta',write=0)
        #xi024[:,1:]=2.0*xi024[:,1:]
        if(0):
            print(xismu.shape)
            pl.figure()
            pl.pcolormesh(rperarr,rpararr,np.log10(wp_xy))
            pl.colorbar()
            pl.figure()
            for kk in range(0,thetaarr.size):
                pl.plot(rrarr,xismu[:,kk],'-')
            pl.xscale('log')

            pl.figure()
            for kk in range(0,rrarr.size):
                pl.plot(thetaarr,xismu[kk,:],'-')

            pl.figure()
            pl.plot(xi024[:,0],np.power(xi024[:,0],2)*xi024[:,1],'s-')
            pl.plot(xi024[:,0],np.power(xi024[:,0],2)*xi024[:,2],'s-')
            pl.plot(xi024[:,0],np.power(xi024[:,0],2)*xi024[:,3],'s-')

        if(ii==-1):
            nwp=wp1d.size; nsxi=xi024.shape[0]
            nwpxis024=np.int(nwp+3*nsxi)
            wpxis024=np.zeros(nwpxis024*(njn+4)).reshape(nwpxis024,njn+4)
            rrwpxi=np.concatenate((rperarr,xi024[:,0],xi024[:,0],xi024[:,0]))
            wpxis024[:,0]=rrwpxi
        else:
            cm='k'; alpha=0.1

        #collect the wp and xi in one matrix
        wpxis024[:,ii+4]=np.concatenate((wp1d,xi024[:,1],xi024[:,2],xi024[:,3]))

    #now compute the mean and variance
    wpxis024[:,1]=np.mean(wpxis024[:,4:],axis=1)
    wpxis024[:,2]=np.std(wpxis024[:,4:],axis=1)*np.sqrt((njn-1)*1.0)

    #save the computed wpxis024 in one file
    with file(outputfilename,'w') as fout:
        fout.write('#wp xi0 xi2 xi4 with jacknife NJN=%d\n'%(njn))
        fout.write('#njn= %d\n'%(njn))
        fout.write('#nwp= %d\n'%(nwp))
        fout.write('#nsxi= %d\n'%(nsxi))
        fout.write('#r(Mpc/h) mean sigma All jacknifecolumns\n')
        np.savetxt(fout,wpxis024,fmt='% 18.8lf')
    print('***********\nwritten: %s \n*************'%(outputfilename))
    return 0
    
def auto_cross_xi024_file(xi02_auto, xi02_cross, output):
    xi02file_auto=np.loadtxt(xi02_auto)
    xi02file_cross=np.loadtxt(xi02_cross)
    outputfile=np.append(xi02file_auto,xi02file_cross,axis=0)
    #look at the njn, nwp, nsxi values of auto (assumed to be same as cross)
    flines=open(xi02_auto,'r').readlines()
    for tline in flines:
        if('#njn=' in tline):
            njn=np.int(tline.split()[1])
        elif('#nwp=' in tline):
            nwp=np.int(tline.split()[1])
        elif('#nsxi=' in tline):
            nsxi=np.int(tline.split()[1])
    with file(output,'w') as fout:
        fout.write('#wp xi0 xi2 xi4 with jacknife NJN=%d\n'%(njn))
        fout.write('#njn= %d\n'%(njn))
        fout.write('#nwp= %d\n'%(nwp))
        fout.write('#nsxi= %d\n'%(nsxi))
        fout.write('#r(Mpc/h) mean sigma All jacknifecolumns\n')
        np.savetxt(fout,outputfile,fmt='% 18.8lf')
    print('***********\nwritten: %s \n*************'%(output))
    return 0
    
    
def process_xi024(file_name, plot=0, covariance=2):
    '''
    covariance: 1=wp_only, 2=xi024_only, 3=wp+xi024
    '''
        
    data=np.loadtxt(file_name)
    L = data.shape
    
    wpbin = L[0]-3*25
    njn = L[1]-4
    
    if plot==1:
        for ii in range(0,3):
            bi=wpbin+ii*25
            ei=bi+25
            pl.errorbar(data[bi:ei,0],data[bi:ei,1]*(data[bi:ei,0]**2),\
                        yerr=data[bi:ei,2]*(data[bi:ei,0]**2), color='b', ls = 'dotted')
            if ii == 0:
                pl.legend()
            #for jj in range(3,54):
                #pl.plot(data[bi:ei,0],data[bi:ei,jj]*(data[bi:ei,0]**2),'k-',alpha=0.01)
        pl.title('$\\xi_{0,2,4}$')

        pl.xlabel('$R_p\,[h^{-1}$Mpc]')
        pl.ylabel('$R_p^2\\xi_{0,2,4}$')
    
    if covariance!=0:
        if covariance==1:
            Cov = np.cov(data[:wpbin,4:])*(njn-1)
            title='wp'

        if covariance==2:
            Cov = np.cov(data[wpbin:,4:])*(njn-1)
            title='xi024'

        if covariance==3:
            Cov = np.cov(data[:,4:])*(njn-1)
            title='wp+xi024'

        Corr = corr_mat(Cov)

              
        pl.figure()
        fig, axarr = pl.subplots(1,2, figsize=[10,4])
        pl.sca(axarr[0])
        im = pl.imshow(Cov)
        cbar = pl.colorbar(im)
        cbar.set_label('$\log$(Cov)', rotation=270)
        pl.title('Covariance: '+title)

        pl.sca(axarr[1])
        im = pl.imshow(Corr, vmax=1, vmin=-1)
        pl.colorbar(im)
        pl.title('Correlation: '+title)
        
        return Cov, Corr

    
def Gelman_Rubin_test(mcchains, nwalkers, steps, paramcol):
    #mcchain: the txt file
    #nwalkers: number of walkers
    #steps: number of steps of each walker
    #paramcol: column of the parameter that needs to be tested
        
    walkers = {}

    for ii in range(nwalkers):
        walkers[ii] = mcchains[ii::nwalkers, paramcol]
    
    mean = np.zeros(nwalkers)
    s2 = np.zeros(nwalkers)
    for ii in range(nwalkers):
        mean[ii] = np.mean(walkers[ii])
        s2[ii] = 1/(np.float(steps)-1)*sum((walkers[ii]-mean[ii])**2)
    
    mean_walkers = np.mean(mean)
    W = np.mean(s2)
    B = np.float(steps)/(nwalkers-1)*sum((mean-mean_walkers)**2)
    var = (1-1/float(steps))*W+1/float(steps)*B
    R = np.sqrt(var/W)

    return R


def Plot_wpxi024(wpxi024):
    fig,axarr=pl.subplots(1,2,figsize=[10,5])
    nrwp = 40
    nrxi = 25

    pl.sca(axarr[0])
    pl.plot(wpxi024[:nrwp,0], wpxi024[:nrwp,1])
    pl.xlim([1,30])
    #pl.xscale('log')
    pl.yscale('log')
    pl.xlabel('$r\,[h^{-1}$Mpc]')
    pl.ylabel('$w_p$')
    pl.legend()

    pl.sca(axarr[1])
    Plot_xi024(wpxi024)
    return 0


def Plot_xi024(wpxi024, fmt='-', errbar=False, xlab=True, ylab=True):
    nrwp = 40
    nrxi = 25
    for ii in range(3):
        a = ii*nrxi+nrwp
        b = (ii+1)*nrxi+nrwp
        if errbar==False:
            pl.plot(wpxi024[a:b,0], wpxi024[a:b,1]*wpxi024[a:b,0]**2, fmt, color='C%d'%ii)
        elif errbar==True:
            pl.errorbar(wpxi024[a:b,0], wpxi024[a:b,1]*wpxi024[a:b,0]**2, yerr=wpxi024[a:b,2]*wpxi024[a:b,0]**2, fmt=fmt, color='C%d'%ii)
    if xlab==True:
        pl.xlabel('$r\,[h^{-1}$Mpc]')
    if ylab==True:
        pl.ylabel('$r^2\,\\xi_{0,2,4}\,[(h^{-1}$Mpc$)^2]$')
    return 0

#@@@@@Covariance matrices(Auto corr edition)@@@@@#
#@cov1: covariance from each mock JN, then average, same as averaging over 1350 JN
def cov1():
    cov = np.zeros((115,115))
    for ii in range(25):
        mockjn = np.loadtxt('/disk2/qhang/GAMA3/Stacpolly/XI02/Galauto_LCv%d-xi02-rp-pi-NJN-54.txt'%ii)
        covsamp = np.cov(mockjn[:,4:], bias=True)*53.0
        cov += covsamp/25.0        
    return cov
#@cov2: covariance from 25 mocks, no JN
def cov2():
    datamatrix = np.zeros((115,25))
    for ii in range(25):
        datamatrix[:,ii] = np.loadtxt('/disk2/qhang/GAMA3/Stacpolly/XI02/Galauto_LCv%d-xi02-rp-pi-NJN-54.txt'%ii)[:,3]
    cov = np.cov(datamatrix, bias=True)
    #Find the non-zero eigenvalues and eigenvectors
    eigval, eigvec = np.linalg.eig(cov)
    useidx = eigval>0.001
    eigval = np.real(eigval[useidx])
    eigvec = np.real(eigvec[:,useidx])
    return cov, eigval, eigvec

#@cov3: covariance from 1350 JN
def cov3():
    datamatrix = np.zeros((115,1350))
    for ii in range(25):
        datamatrix[:,ii*54:(ii+1)*54] = np.loadtxt('/disk2/qhang/GAMA3/Stacpolly/XI02/Galauto_LCv%d-xi02-rp-pi-NJN-54.txt'%ii)[:,4:]
    cov = np.cov(datamatrix, bias=True)
    return cov


def window_function(k,r):
    return 3*(np.sin(k*r)/(k*r)**2-np.cos(k*r)/(k*r))/(k*r)

def radius(m):
    M=m
    rho=2.775e11*0.27
    return (3*M/(4*np.pi*rho))**(1/3.0)

def growth_factor(z,Omega_m0=0.27):
    a=1/(1+z)
    #Omega_m0=0.3
    Omega_m=Omega_m0*a**(-3)/(Omega_m0*a**(-3)+1-Omega_m0)
    x=(1-Omega_m)**(1/3.0)
    g=x*(1-x**1.91)**0.82+1.437*(1-(1-x**3)**(2/3.0))
    x0=(1-Omega_m0)**(1/3.0)
    g0=x0*(1-x0**1.91)**0.82+1.437*(1-(1-x0**3)**(2/3.0))
    return g/g0

def Mass_dep_bias_PS(m,z):
    Pdata=np.loadtxt('/disk1/qhang/GAMA/Halofit_sig8/halofitz=0sig8=08.dat')
    k=Pdata[:,0]
    P_lin=Pdata[:,1]   
    lnk=np.log10(k)
    dlnk = lnk[1]-lnk[0]
    r=radius(m)
    I=sum(P_lin*window_function(k,r)**2*dlnk/np.log10(np.exp(1)))
    sig=np.sqrt(I)*growth_factor(z)
    nu=1.686/sig
    bias=1+(nu**2-1)/1.686
    return bias

#Using a more accurate fitting formula

def more_accurate_bias(sig0,z):
    sig=sig0*growth_factor(z)
    nu=1.686/sig
    a=1.529
    b=0.704
    c=0.412
    term1=(a*b**2)*nu**b+4*c*nu**2+2*c*a*(2+b)*nu**(b+2)
    term2=(a*b)*nu**b+2*c*nu**2*(1+a*nu**b)
    term3=(2*a*b*nu**b)/(1+a*nu**b)
    term4=2*c*nu**2
    return 1-(term1/term2-term3-term4)/1.686

def F_function(nu):
    a=1.529
    b=0.704
    c=0.412
    F=(1+a*nu**b)**(-1)*np.exp(-c*nu**2)
    return F

def G_function(nu):
    a=1.529
    b=0.704
    c=0.412
    G=(a*b*nu**b+2*c*nu**2*(1+a*nu**b))/(1+a*nu**b)**2*np.exp(-c*nu**2)
    return G

def G_PS_function(nu):
    return np.sqrt(2/np.pi)*nu*np.exp(-nu**2/2.0)

def Mass_dep_bias(m,z,fitform='Peacock'):
    #m can be an array
    Pdata=np.loadtxt('/disk1/qhang/GAMA/Halofit_sig8/halofitz=0sig8=08.dat')
    k=Pdata[:,0]
    P_lin=Pdata[:,1]   
    lnk=np.log10(k)
    dlnk = lnk[1]-lnk[0]
    r=radius(m)
    km=np.outer(k,np.ones(len(r)))
    rm=np.outer(np.ones(len(k)),r)
    P_linm=np.outer(P_lin,np.ones(len(r)))
    sig2=sum(P_linm*window_function(km,rm)**2*dlnk/np.log10(np.exp(1)))
    if fitform=='Peacock':
        bias=more_accurate_bias(np.sqrt(sig2),z)
    elif fitform=='Tinker10':
        nu=1.686/(np.sqrt(sig2)*growth_factor(z))
        bias=bias_tinker10(nu)
    return bias

def bias_tinker10(nu):
    y=np.log10(200)
    A=1.0+0.24*y*np.exp(-(4/y)**4)
    a=0.44*y-0.88
    B=0.183
    b=1.5
    C=0.019+0.107*y+0.19*np.exp(-(4/y)**4)
    c=2.4
    deltac=1.686
    bias=1-A*(nu**a)/(nu**a+deltac**a)+B*nu**b+C*nu**c
    return bias

def Kat_mass_sel(mass,Massbin):   
    
    #Now mass bin indicates the percentage
    pc1 = 0.4
    pc2 = 0.9
    #For selecting the group masses
    Nsel1 = int(pc1*len(mass))
    Nsel2 = int(pc2*len(mass))
    
    #Sort the mass
    sortedidx = np.argsort(mass)
    if(Massbin==1):
        index = sortedidx[:Nsel1]
    if(Massbin==2):
        index = sortedidx[Nsel1:Nsel2]
    if(Massbin==3):
        index = sortedidx[Nsel2:] 
    #This index can only be applied on the z-selected mass
    return index

def get_group_bias(halomass,stellarmass,z,bins=50,fitform='Peacock'):
    loghm=np.log10(halomass)
    #all groups case
    meanz=np.mean(z)
    counts,binedges=np.histogram(loghm,bins=bins)
    b_edges=Mass_dep_bias(10**binedges,meanz,fitform=fitform)
    b=((b_edges+np.roll(b_edges,1))/2.)[1:]
    b_all=sum(b*counts)/float(sum(counts))
    #separate mass bins
    LFCmass=stellarmass*np.exp((z/0.33)**2)
    #LM
    LMind=Kat_mass_sel(LFCmass,1)
    meanz=np.mean(z[LMind])
    counts,binedges=np.histogram(loghm[LMind],bins=bins)
    b_edges=Mass_dep_bias(10**binedges,meanz,fitform=fitform)
    b=((b_edges+np.roll(b_edges,1))/2.)[1:]
    b_LM=sum(b*counts)/float(sum(counts))
    #MM
    MMind=Kat_mass_sel(LFCmass,2)
    meanz=np.mean(z[MMind])
    counts,binedges=np.histogram(loghm[MMind],bins=bins)
    b_edges=Mass_dep_bias(10**binedges,meanz,fitform=fitform)
    b=((b_edges+np.roll(b_edges,1))/2.)[1:]
    b_MM=sum(b*counts)/float(sum(counts))
    #HM
    HMind=Kat_mass_sel(LFCmass,3)
    meanz=np.mean(z[HMind])
    counts,binedges=np.histogram(loghm[HMind],bins=bins)
    b_edges=Mass_dep_bias(10**binedges,meanz,fitform=fitform)
    b=((b_edges+np.roll(b_edges,1))/2.)[1:]
    b_HM=sum(b*counts)/float(sum(counts))
    
    return b_all, b_LM, b_MM, b_HM
