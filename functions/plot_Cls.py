import pylab as pl
import matplotlib
matplotlib.rcParams.update({'font.size': 12, 'font.family': 'serif','axes.linewidth':1})

import numpy as np

def plot_cl(data,zlab,plot_theory=False,clth=None,lmax=300,cltype='',save=False,savedir='',unbin=True):
    '''
    cltype: 'clgg', 'clgk', 'clgt'
    assumes data and theory have the same structure
    '''
    indmax=lmax/10
    ell=data['ell'][:indmax]
    if unbin==False:
        nplot=len(zlab)-1
    if unbin==True:
        nplot=len(zlab)
    if cltype=='clgg':
        #plot 10 auto-cross correlations
        fig,axarr=pl.subplots(nplot,nplot,figsize=[13,13])
               
        for ii in range(nplot):
            for jj in range(nplot):
                if ii>jj:
                    axarr[ii,jj].axis('off')
                if ii==jj:
                    key='%d%d'%(ii,ii)
                    pl.sca(axarr[ii,ii])
                    pl.errorbar(ell,data[key][:indmax,0],yerr=data[key][:indmax,1],fmt='.',markersize=10)
                    if plot_theory==True:
                        pl.plot(ell,clth[key][:indmax],'k')
                    pl.ylabel('$\\ell(\\ell+1)C^{gg}_{\\ell}/2\\pi$',fontsize=15)
                    pl.xlabel('$\\ell$',fontsize=15)
                    pl.ylim([0,data['00'][indmax-1,0]*1.1])

                if ii<jj:
                    key='%d%d'%(ii,jj)
                    pl.sca(axarr[ii,jj])
                    pl.errorbar(ell,data[key][:indmax,0],yerr=data[key][:indmax,1],fmt='.',markersize=10)
                    if plot_theory==True:
                        pl.plot(ell,clth[key][:indmax],'k')
                    if jj<nplot-1:
                        pl.yticks([])
                    pl.xticks([])
                    pl.ylim([-0.1,1.1])
                    if jj==nplot-1:
                        pl.yticks([0,0.2,0.4,0.6,0.8,1.0])
                        pl.ylabel('$C^{12}_{\\ell}/\\sqrt{C^{1}_{\\ell}C^{2}_{\\ell}}$',fontsize=15)
                        axarr[ii,jj].yaxis.tick_right()
                        axarr[ii,jj].yaxis.set_label_position("right")
        pl.subplots_adjust(wspace=0,hspace=0)

    if cltype=='clgk':
        fig,axarr=pl.subplots(1,nplot,figsize=[13,3],sharex=True,sharey=True)
        for ii in range(nplot):
            pl.sca(axarr[ii])
            key='clgk%d'%ii
            pl.errorbar(ell,data[key][:indmax,0],yerr=data[key][:indmax,1],fmt='.',markersize=10)
            if plot_theory==True:
                pl.plot(ell,clth[key][:indmax],'k')
            if unbin==False:
                pl.title('$%s<z<%s$'%(zlab[ii],zlab[ii+1]))
            if unbin==True:
                if ii<nplot-1:
                    pl.title('$%s<z<%s$'%(zlab[ii],zlab[ii+1]))
                if ii==nplot-1:
                    pl.title('Unbinned')
            pl.xlabel('$\\ell$')
            if ii==0:
                pl.ylabel('$\\ell(\\ell+1)C^{g\\kappa}_{\\ell}/2\\pi$')
        ymax=data['clgk3'][indmax-1,0]
        pl.ylim([0,ymax*1.5])
        pl.subplots_adjust(wspace=0)
    
    if cltype=='clgt':
        fig,axarr=pl.subplots(1,nplot,figsize=[13,3],sharey=True,sharex=True)
        for ii in range(nplot):
            pl.sca(axarr[ii])
            key='clgt%d'%ii
            pl.errorbar(ell,data[key][:indmax,0]*1e6,yerr=data[key][:indmax,1]*1e6,fmt='.',markersize=10)
            if plot_theory==True:
                pl.plot(ell,clth[key][:indmax]*1e6,'k')
            if ii==0:
                pl.ylabel('$\\ell(\\ell+1)C^{gT}_{\\ell}/2\\pi$ [$\\mu$K]')
            pl.xlabel('$\\ell$')
            if unbin==False:
                pl.title('$%s<z<%s$'%(zlab[ii],zlab[ii+1]))
            if unbin==True:
                if ii<nplot-1:
                    pl.title('$%s<z<%s$'%(zlab[ii],zlab[ii+1]))
                if ii==nplot-1:
                    pl.title('Unbinned')
        pl.subplots_adjust(wspace=0)
    
    if save==True:
        pl.savefig(savedir,bbox_inches="tight")
    pl.show()
    #pl.close()
    return 0
